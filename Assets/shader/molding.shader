﻿Shader "Custom/molding" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

		_MainTex ("메인", 2D) = "white" {}
		_BumpMap ("메인노말", 2D) = "bump"{}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_MainTex2 ("윗부분 몰딩", 2D) = "white" {}
		_BumpMap2 ("윗부분 노말", 2D) = "bump"{}
		
		_MainTex3 ("가운데", 2D) = "white" {}
		_BumpMap3 ("가운데노말", 2D) = "bump"{}

		_NM ("노말값 조절" , Float ) = 1
		
		
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
				
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MainTex2;
		sampler2D _MainTex3;
		sampler2D _BumpMap ; 
		sampler2D _BumpMap2 ;
		sampler2D _BumpMap3 ;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
			float2 uv_MainTex3;
			float2 uv_BumpMap ;
			float2 uv_BumpMap2 ;
			float2 uv_BumpMap3 ;
			float4 color:COLOR ;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _NM ; 
		

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 d = tex2D (_MainTex2, IN.uv_MainTex2) * _Color;
			fixed4 e = tex2D (_MainTex3, IN.uv_MainTex3) * _Color;

			float3 n = UnpackNormal (tex2D (_BumpMap, IN.uv_MainTex))*_NM;
			float3 n2 = UnpackNormal (tex2D (_BumpMap2, IN.uv_MainTex2)) ;
			float3 n3 = UnpackNormal (tex2D (_BumpMap3, IN.uv_MainTex3));

			//float3 FN = (IN.color.r * n3.rg * _NM, n3.b );
			
			o.Normal = n;
			o.Normal = lerp (n , n2 , IN.color.b) ;
			o.Normal = lerp (o.Normal , n2 , IN.color.b) ;
			o.Normal = lerp (o.Normal, n3, IN.color.r) ;
			//o.Normal = float3 (IN.color.r * n3.r * _NM, IN.color. * n3.g * _NM, n3.b ) ;
			

			o.Albedo = lerp(c.rgb, d.rgb, IN.color.b) ;
			o.Albedo = lerp(o.Albedo, e.rgb, IN.color.r ) ; 
			o.Metallic =  _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
