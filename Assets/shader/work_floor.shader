﻿Shader "jackpot/work_floor" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MainTex2 ("Albedo (RGB)", 2D) = "white" {}
		_MainTex3 ("Alpha", 2D) = "white" {}

		_Blood ("Blood", Range(0,1)) = 1
		_Glossiness ("Smoothness", Range(0,1)) = 1
		_Glossinessmap ("Smoothness", 2D) = "black" {}

		_Glossiness2 ("bloodSmoo", Range(0,1)) = 1
		_Glossinessmap2 ("bloodSmoomap", 2D) = "black" {}

		_Bloodfloat ("Bloodfloat", Range(0,1)) = 0.5
		
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MetallicGlossmap ("Metallic", 2D) = "black" {}

		_Bumpmap("Normal",2D)= "bump"{}
		_NMfloat("노말강도", float) = 1
		_OcclusionMap ("AO", 2D) = "white" {}
		_OcclusionStrength ("AO*n", float) =1


	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 4.0

		sampler2D _MainTex;
		sampler2D _MainTex2;
		sampler2D _MainTex3;
		sampler2D _Glossinessmap;
		sampler2D _Glossinessmap2;
		sampler2D _MetallicGlossmap;		
		sampler2D _Bumpmap;
		sampler2D _OcclusionMap;





		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
			float2 uv_MainTex3;
			float2 uv_Glossinessmap;
			float2 uv_Glossinessmap2;
			float2 uv_MetallicGlossmap;
			float2 uv_Bumpmap;			
			float2 uv_OcclusionMap;


		};

		float _Glossiness ,_Glossiness2, _Bloodfloat ; 
		float _Blood;
		float _Metallic,_OcclusionStrength, _NMfloat;
		float4 _Color;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			float4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			float4 d = tex2D (_MainTex2, IN.uv_MainTex2) * _Color;

			float4 blood = tex2D (_MainTex3, IN.uv_MainTex3) * _Color;
			float4 bla = blood.a;

			//float4 e =  d.a  * _Blood; 
			float4 e =  (d.a * bla) * _Blood; 

			float4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap) * _Color;

			float4 ga = tex2D (_Glossinessmap2, IN.uv_Glossinessmap2) * _Color;

			float4 m = tex2D (_MetallicGlossmap, IN.uv_MetallicGlossmap) * _Color;
			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));

			float4 ao = tex2D (_OcclusionMap, IN.uv_OcclusionMap);

			


			
			o.Albedo = c * d ;
			//o.Albedo = lerp(c,d,e);
			o.Metallic = (m*_Metallic).rgb;
			o.Smoothness =  saturate ( ((1 - g) *_Glossiness).rgb ) + saturate ( ((1 - ga) * _Glossiness2).rgb ) + e ;

			o.Alpha = c.a;
			//o.Normal = normalmap ; 
			o.Normal =  float3(normalmap.r * _NMfloat, normalmap.g * _NMfloat, normalmap.b );
			o.Occlusion = pow(ao, _OcclusionStrength);


		}
		ENDCG
	}
	FallBack "Diffuse"
}
