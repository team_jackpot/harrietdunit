﻿Shader "jackpot/floor" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_MainTex2 ("Albedo (RGB)", 2D) = "white" {}
		_dirty ("dirt", float) = 1

		_Glossiness ("Smoothness", Range(0,1)) = 1
		_Glossinessmap ("Smoothness", 2D) = "black" {}

		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MetallicGlossmap ("Metallic", 2D) = "black" {}

		_Bumpmap("Normal",2D)= "bump"{}
		_N ("N*n", float) = 1
		_OcclusionMap ("AO", 2D) = "white" {}
		_OcclusionStrength ("AO*n", float) =1
		_OcclusionMap2 ("AO2", 2D) = "white" {}
		_OcclusionStrength2 ("AO2*n", float) =1


	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 4.0

		sampler2D _MainTex, _MainTex2;
		sampler2D _Glossinessmap;
		sampler2D _MetallicGlossmap;		
		sampler2D _Bumpmap;
		sampler2D _OcclusionMap;
		sampler2D _OcclusionMap2;





		struct Input {
			float2 uv_MainTex, uv_MainTex2;
			float2 uv_Glossinessmap;
			float2 uv_MetallicGlossmap;
			float2 uv_Bumpmap;			
			float2 uv_OcclusionMap;
			float2 uv_OcclusionMap2;


		};

		float _Glossiness, _dirty ;
		float _Metallic,_OcclusionStrength, _OcclusionStrength2, _N;
		float4 _Color;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			float4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			float4 d = tex2D (_MainTex2, IN.uv_MainTex2) * _Color;
			float4 e = d.a * _dirty ;

			float4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap) * _Color;
			float4 m = tex2D (_MetallicGlossmap, IN.uv_MetallicGlossmap) * _Color;
			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));
			float4 ao = tex2D (_OcclusionMap, IN.uv_OcclusionMap);
			float4 ao2 = tex2D (_OcclusionMap2, IN.uv_OcclusionMap2);



			o.Albedo = lerp(c,d,e) ;
			o.Metallic = (m*_Metallic).rgb;
			o.Smoothness = ((1 - g) *_Glossiness).rgb; 
			o.Alpha = c.a;
			o.Normal =  float3 (normalmap.r * _N, normalmap.g * _N, normalmap.b);
			o.Occlusion = pow(ao, _OcclusionStrength);
			o.Occlusion = o.Occlusion * pow(ao2, _OcclusionStrength2) ; 


		} 
		ENDCG
	}
	FallBack "Diffuse"
}
