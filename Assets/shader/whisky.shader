﻿Shader "Custom/whisky" {
	Properties {
        _Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		GrabPass {}

		CGPROGRAM
		#pragma surface surf  Lambert
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _GrabTexture;

		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};

		float4 _Color;


		
		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex)  * _Color ;

			float3 screenUV = IN.screenPos.xyz / IN.screenPos.w;

			o.Emission = tex2D(_GrabTexture, screenUV.xy + c * 0.1); 			
			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
