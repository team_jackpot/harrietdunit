﻿Shader "jackpot/standard-roughness" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 1
		_Glossinessmap ("Smoothness", 2D) = "black" {}

		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MetallicGlossmap ("Metallic", 2D) = "black" {}

		_Bumpmap("Normal",2D)= "bump"{}
		_OcclusionMap ("AO", 2D) = "white" {}
		_OcclusionStrength ("AO*n", float) =1


	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Glossinessmap;
		sampler2D _MetallicGlossmap;		
		sampler2D _Bumpmap;
		sampler2D _OcclusionMap;





		struct Input {
			float2 uv_MainTex;
			float2 uv_Glossinessmap;
			float2 uv_MetallicGlossmap;
			float2 uv_Bumpmap;			
			float2 uv_OcclusionMap;


		};

		float _Glossiness;
		float _Metallic,_OcclusionStrength;
		float4 _Color;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			float4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			float4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap) * _Color;
			float4 m = tex2D (_MetallicGlossmap, IN.uv_MetallicGlossmap) * _Color;
			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));
			float4 ao = tex2D (_OcclusionMap, IN.uv_OcclusionMap);



			o.Albedo = c.rgb;
			o.Metallic = (m*_Metallic).rgb;
			o.Smoothness = ((1 - g) *_Glossiness).rgb; 
			o.Alpha = c.a;
			o.Normal =  normalmap;
			o.Occlusion = pow(ao, _OcclusionStrength);


		}
		ENDCG
	}
	FallBack "Diffuse"
}
