﻿Shader "Custom/smoothnesss" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_RefPow ("Refraction Distortion", Range(0,128)) = 15.0

		_RimColor("Rimcolor", Color) = (1,1,1,1)
		_RimPower("Rimpower", Range(1,10)) = 3
	}
	SubShader {
		GrabPass {"_Refraction"}
		Tags { "RenderType"="Transparent"  "Queue" = "Transparent"}
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha:blend
		
		//sampler2D _MainTex;
		sampler2D _Refraction;
		float4 _Refraction_TexelSize;

		struct Input {
			//float2 uv_MainTex;
			float4 screenPos;
			float3 worldRefl;
			float3 viewDir;
		};

		half _Glossiness;
		half _RefPow;
		float4 _Color, _RimColor;
		float _RimPower;

		float4 LightingGem (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {

            float3 h = normalize (viewDir + lightDir);
            float nh = max (0, dot (normalize (s.Normal), h));
            float spec = pow (nh, (pow (s.Specular, 2) * 256.0) + 0.1) * s.Specular * 2;
 
            half4 c;
            c.rgb = spec * _LightColor0 * atten;
            c.a = s.Alpha;
            return c; 
        }		

		void surf (Input IN, inout SurfaceOutputStandard o) {

			half2 offset = o.Normal * _RefPow * _Refraction_TexelSize.xy;
            IN.screenPos.xy /= IN.screenPos.w;
            IN.screenPos.xy = IN.screenPos.z * offset*10 + IN.screenPos.xy;
			float rim = max(0,dot(o.Normal,IN.viewDir));
            o.Emission = (tex2D (_Refraction, IN.screenPos.xy) + DecodeHDR (UNITY_SAMPLE_TEXCUBE (unity_SpecCube0, IN.worldRefl), unity_SpecCube0_HDR)) * 1.0* _Color+pow(1-rim,_RimPower)*_RimColor.rgb*3;
 
            o.Smoothness = _Glossiness;
            o.Alpha = _Color.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
