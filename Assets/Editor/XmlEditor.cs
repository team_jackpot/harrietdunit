﻿//------------------------------------------------------------------//
// 작성자 : 조성혜
// 엑셀에서 파싱한 xml 파일을 지정된 서식으로 수정하는 Editor
// 최종 수정일자 : 18.09.02
//------------------------------------------------------------------//
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System;

// 단서 테이블 구조체
struct NoteTable
{          
    public string id;               
    public string evidenceClass;    
    public string DefaultState;
    
    public string evidenceNameKr;
    public string EvidenceDefaultName;
    public string EvidenceDesc;

    public string OnStateID;
    public string OffStateID;
}
// 단서 구분 목록
enum EvidenceClass
{
    Char,
    Clue
}

// 오브젝트 조사 테이블 구조체
struct ObserveTable
{
    //nextScene, Event id 추가 해야함 
    public string scene;
    public string type;
    public string id;
    public string objName;
    public string defaultState;
    public string priority;
    public string chName;
    public string chText;
    public string nextId;

    public string eventName;
    public string hearing;
    public string bgName;
    public string getOnID;
    public string getOffID;
    public string getAniName;
    public string chSpwan;
    public string CamCtrl;
}

// 선택지 테이블 
struct SelectTable
{
    public string id;
    public string sceneName;
    public string defaultState;
    public string chName;
    public string selectText;
    public string nextID;
    public string nextScene;
}

//퀘스트
struct QuestMain
{
    public string scene;
    public string id;
    public string DefalutState;

    public string questUIText;
    public string MonologueID;

    public string GetOffStateID;
    public string GetOnStateID;
    public string NextSceneName;
    public string ChSpwan;
}
struct QuestSub
{
    public string ID;
    public string SceneName;
    public string QuestGroupID;

    public string QuestScript;
    public string GetProvisoID;
}

//확대조사 
struct EventTable
{
    public string eventName;
    public string CameraName;
    public string QuestListUnlock;
    public string GetOnStateID;
}

struct CriminalTable
{
    public string CriminalType;
    public string ID;
    public string ScriptChName;
    public string ScriptChText;
    public string ScriptNextID;
    public string GroupID;
    public string MonoScriptID;
    public string DownLife;
    public string CamControl;
    public string AniControl;
    public string BGNameTrans;
    public string ChSpwan;
}


// 씬 목록
enum Scene
{
    none,
    Tu_Library,
    P_Room,
    Street,
    Doc_Room,
    Library_Night,
    Doc_Room_Night,
    Street_Night,
    Modo_Night,
    P_Room_Night
}
enum DialType
{
    OBJ,
    NPC,
    DET,
    EXP,
    SEL,
    ALO,
    ETC
}

public class XmlEditor : EditorWindow
{
 

    // 엑셀에서 파싱한 xml
    TextAsset tableXml;

    // 엑셀에서 파싱한 xmlDocument
    XmlDocument orignDoc;

    //-------------- 구조체 리스트 ----------------
    List<NoteTable> noteList;       // 단서
    List<ObserveTable> obList;              // 오브젝트
    List<SelectTable> selectList;           // 선택지 
    List<QuestMain> qList;
    List<QuestSub> subList;
    List<EventTable> eventList;

    // 복사에 사용할 xmlDocument
    XmlDocument copyDoc;

    // 메뉴 생성
    [MenuItem("XmlMaker/ModifyXml")]

    static void Open()
    {
        GetWindow<XmlEditor>();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("단서 테이블", EditorStyles.boldLabel);
        tableXml = (TextAsset)EditorGUILayout.ObjectField("TableXml", tableXml, typeof(TextAsset), true);

        EditorGUILayout.LabelField("서식 변경", EditorStyles.boldLabel);

        // -------- 가로 맞춤 시작 -------
        GUILayout.BeginHorizontal();

        //버튼 클릭시 단서 xml 서식 변경
        if (GUILayout.Button("단서", GUILayout.Width(70), GUILayout.Height(30)))
        {
            NoteXml();
        }

        if (GUILayout.Button("탐색", GUILayout.Width(70), GUILayout.Height(30)))
        {
            // 오브젝트 테이블 변경
            ObXml();
        }

        if (GUILayout.Button("메인퀘스트", GUILayout.Width(70), GUILayout.Height(30)))
        {
            QLXml();
        }
        if (GUILayout.Button("서브퀘스트", GUILayout.Width(70), GUILayout.Height(30)))
        {
            QTXml();
        }
        if (GUILayout.Button("선택", GUILayout.Width(70), GUILayout.Height(30)))
        {
            DialXml();
        }
        EditorGUILayout.EndHorizontal();
        
        if (GUILayout.Button("이벤트", GUILayout.Width(70), GUILayout.Height(30)))
        {
            EventXml();
        }

        if (GUILayout.Button("범인지목", GUILayout.Width(70), GUILayout.Height(30)))
        {
            CriminalXml();
        }

        if (GUILayout.Button("지목생각", GUILayout.Width(70), GUILayout.Height(30)))
        {
            CrimGroupXml();
        }

        // -------- 가로 맞춤 끝 ---------
       // EditorGUILayout.EndHorizontal();

    }

    // 단서 xml 변경
    private void NoteXml()
    {
        noteList = new List<NoteTable>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("Note");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/Note.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("Note").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            NoteTable copy = new NoteTable();

            copy.id = node.Attributes.GetNamedItem("ID").Value;
            copy.evidenceClass = node.Attributes.GetNamedItem("EvidenceClass").Value;
            copy.DefaultState = node.Attributes.GetNamedItem("DefaultState").Value;

            
            copy.evidenceNameKr = node.Attributes.GetNamedItem("EvidenceNameKr").Value;

            copy.EvidenceDesc = null;
            if (node.Attributes["EvidenceDesc"] != null)
                copy.EvidenceDesc = node.Attributes["EvidenceDesc"].Value;

            copy.EvidenceDefaultName = null;
            if(node.Attributes["EvidenceDefaultName"] != null)
                copy.EvidenceDefaultName = node.Attributes.GetNamedItem("EvidenceDefaultName").Value;

            copy.OnStateID = null;
            if (node.Attributes["OnStateID"] != null)
                copy.OnStateID = node.Attributes["OnStateID"].Value;

            copy.OffStateID = null;
            if (node.Attributes["OffStateID"] != null)
                copy.OffStateID = node.Attributes["OffStateID"].Value;

            noteList.Add(copy);
        }

        //------------- 붙여넣기 ----------------//

        // 씬 노드 아래 단서 클래스 별 노드 생성
        foreach (string className in Enum.GetNames(typeof(EvidenceClass)))
        {
            XmlElement classElement = copyDoc.CreateElement(className);
            root.AppendChild(classElement);
        }

        foreach (NoteTable note in noteList)
        {
            // 해당 단서 클래스 노드 선택
            XmlNode classNode = copyDoc.SelectSingleNode("Note/" + note.evidenceClass);
       
            // 단서별 노드 생성
            XmlElement nameElement = copyDoc.CreateElement("Clue");
            nameElement.SetAttribute("ID", note.id);
            nameElement.SetAttribute("DefaultState", note.DefaultState);

            // 단서 관련 노드 생성
            XmlElement desc = copyDoc.CreateElement("EvidenceDesc");
            desc.InnerText = note.EvidenceDesc;
            XmlElement kr = copyDoc.CreateElement("EvidenceNameKr");
            kr.InnerText = note.evidenceNameKr;
            XmlElement defaultname = copyDoc.CreateElement("EvidenceDefaultName");
            defaultname.InnerText = note.EvidenceDefaultName;

            //단서 이름 노드 아래 자식 노드 붙이기 
            nameElement.AppendChild(kr);
            nameElement.AppendChild(defaultname);
            nameElement.AppendChild(desc);

            XmlElement option = copyDoc.CreateElement("Option");
            if (note.OnStateID != null)
                option.SetAttribute("OnStateID", note.OnStateID);
            if (note.OffStateID != null)
                option.SetAttribute("OffStateID", note.OffStateID);
            nameElement.AppendChild(option);

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            classNode.AppendChild(nameElement);

        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/Note.xml");
    }

    // 오브젝트 대사 xml 변경
    private void ObXml()
    {
        obList = new List<ObserveTable>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        //루트노드
        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("Observe");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/Observe.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("Observe").ChildNodes;

        foreach (XmlNode node in nodeList)
        {
            ObserveTable table;

            table.scene = node.Attributes.GetNamedItem("SceneName").Value;

            table.type = null;
            if (node.Attributes["DialType"] != null)
                table.type = node.Attributes.GetNamedItem("DialType").Value;
            else
                table.type = "ETC";

            table.id = node.Attributes.GetNamedItem("ID").Value;

            table.objName = null;
            if (node.Attributes["ObjNameEng"] != null)
                table.objName = node.Attributes.GetNamedItem("ObjNameEng").Value;

            table.defaultState = null;
            if (node.Attributes["DefaultState"] != null)
                table.defaultState = node.Attributes.GetNamedItem("DefaultState").Value;

            table.priority = null;
            if (node.Attributes["ObjectPriority"] != null)
                table.priority = node.Attributes.GetNamedItem("ObjectPriority").Value;

            table.chName = node.Attributes.GetNamedItem("ScriptChName").Value;
            table.chText = node.Attributes.GetNamedItem("ScriptChText").Value;
            table.nextId = node.Attributes.GetNamedItem("ScriptNextID").Value;

            table.eventName = null;
            table.hearing = null;
            table.bgName = null;
            table.getOnID = null;
            table.getOffID = null;
            table.getAniName = null;
            table.chSpwan = null;
            table.CamCtrl = null;

            if (node.Attributes["NextEventName"] != null)
                table.eventName = node.Attributes.GetNamedItem("NextEventName").Value;
            if (node.Attributes["HearingGroupID"] != null)
                table.hearing = node.Attributes.GetNamedItem("HearingGroupID").Value;

            if (node.Attributes["BGNameTrans"] != null)
                table.bgName = node.Attributes.GetNamedItem("BGNameTrans").Value;

            if (node.Attributes["GetOnStateID"] != null)
                table.getOnID = node.Attributes.GetNamedItem("GetOnStateID").Value;

            if (node.Attributes["GetOffStateID"] != null)
                table.getOffID = node.Attributes.GetNamedItem("GetOffStateID").Value;
            if (node.Attributes["GetAniName"] != null)
                table.getAniName = node.Attributes.GetNamedItem("GetAniName").Value;
            if(node.Attributes["ChSpwan"]!= null)
                table.chSpwan = node.Attributes["ChSpwan"].Value;
            if (node.Attributes["CamCtrl"] != null)
                table.CamCtrl = node.Attributes["CamCtrl"].Value;

            obList.Add(table);
        }


        //------------- 붙여넣기 ----------------//

        // 씬 별로 노드 생성
        foreach (string sceneName in Enum.GetNames(typeof(Scene)))
        {
            XmlElement sceneElement = copyDoc.CreateElement(sceneName);
            foreach (string type in Enum.GetNames(typeof(DialType)))
            {
                XmlElement typeElement = copyDoc.CreateElement(type);
                sceneElement.AppendChild(typeElement);
            }
            root.AppendChild(sceneElement);
        }

        for (int i = 0; i < obList.Count; i++)
        {
            // 해당 오브젝트 씬, 타입 노드 선택
            XmlNode classNode = copyDoc.SelectSingleNode("Observe/" + obList[i].scene + "/" + obList[i].type);

            // 오브젝트 id 노드 + name 
            XmlElement idEle = copyDoc.CreateElement("Info");
            idEle.SetAttribute("ID", obList[i].id);

            if (obList[i].objName != null)
                idEle.SetAttribute("ObjNameEng", obList[i].objName);

            //-------------- 단서 관련 노드 생성 ----------------//

            //State 노드 (DefaultState, Priority)
            XmlElement stateEle = copyDoc.CreateElement("State");
            if (obList[i].defaultState != null)
            {
                stateEle.SetAttribute("DefaultState", obList[i].defaultState);
                stateEle.SetAttribute("ObjectPriority", obList[i].priority);
            }
            idEle.AppendChild(stateEle);

            // 대화 이름
            XmlElement chNameEle = copyDoc.CreateElement("ScriptChName");
            chNameEle.InnerText = obList[i].chName;
            idEle.AppendChild(chNameEle);

            // 대화 내용
            XmlElement chTextEle = copyDoc.CreateElement("ScriptChText");
            chTextEle.InnerText = obList[i].chText;
            idEle.AppendChild(chTextEle);

            // 다음 대화
            XmlElement nextEle = copyDoc.CreateElement("ScriptNextID");
            nextEle.InnerText = obList[i].nextId;
            idEle.AppendChild(nextEle);

            // 옵션 노드 (이벤트,선택지,일러스트,On,Off,애니제어)
            XmlElement opEle = copyDoc.CreateElement("Option");
            if(obList[i].eventName != null)
                opEle.SetAttribute("NextEventName", obList[i].eventName);
            if (obList[i].hearing != null)
                opEle.SetAttribute("HearingGroupID", obList[i].hearing);

            if (obList[i].bgName != null)
                opEle.SetAttribute("BGNameTrans", obList[i].bgName);

            if (obList[i].getOnID != null)
                opEle.SetAttribute("GetOnStateID", obList[i].getOnID);

            if (obList[i].getOffID != null)
                opEle.SetAttribute("GetOffStateID", obList[i].getOffID);
            if (obList[i].getAniName != null)
                opEle.SetAttribute("GetAniName", obList[i].getAniName);
            if(obList[i].chSpwan != null)
                opEle.SetAttribute("ChSpwan", obList[i].chSpwan);
            if (obList[i].CamCtrl != null)
                opEle.SetAttribute("CamCtrl", obList[i].CamCtrl);
            idEle.AppendChild(opEle);

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            classNode.AppendChild(idEle);
        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/Observe.xml");
    }

    // 메인퀘스트 xml 변경
    private void QLXml()
    {
        qList = new List<QuestMain>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("QL");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/QL.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("QL").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            QuestMain main;

            main.scene = node.Attributes["SceneName"].Value;
            main.id = node.Attributes.GetNamedItem("ID").Value;
            main.DefalutState = node.Attributes.GetNamedItem("DefaultState").Value;
            main.questUIText = node.Attributes.GetNamedItem("QuestUIText").Value;

            main.MonologueID = null;
            if (node.Attributes["MonologueID"] != null)
                main.MonologueID = node.Attributes.GetNamedItem("MonologueID").Value;

            main.GetOffStateID = null;
            if(node.Attributes["GetOffStateID"] != null)
                main.GetOffStateID = node.Attributes.GetNamedItem("GetOffStateID").Value;

            main.GetOnStateID = null;
            if (node.Attributes["GetOnStateID"] != null)
                main.GetOnStateID = node.Attributes.GetNamedItem("GetOnStateID").Value;

            main.NextSceneName = null;
            if (node.Attributes["NextSceneName"] != null)
                main.NextSceneName = node.Attributes["NextSceneName"].Value;

            main.ChSpwan = null;
            if (node.Attributes["ChSpwan"] != null)
                main.ChSpwan = node.Attributes["ChSpwan"].Value;

            qList.Add(main);
        }

        //------------- 붙여넣기 ----------------//

        // 씬 별로 노드 생성
        foreach (string sceneName in Enum.GetNames(typeof(Scene)))
        {
            XmlElement sceneElement = copyDoc.CreateElement(sceneName);

            root.AppendChild(sceneElement);
        }

        foreach (QuestMain main in qList)
        {
            // 해당 단서 클래스 노드 선택
            XmlNode classNode = copyDoc.SelectSingleNode("QL/" + main.scene );

            // 퀘스트별 노드 생성
            XmlElement quest = copyDoc.CreateElement("Quest");
            quest.SetAttribute("ID", main.id);
            quest.SetAttribute("DefaultState", main.DefalutState);

            // 관련 노드 생성
            XmlElement questUITextElement = copyDoc.CreateElement("QuestUIText");
            questUITextElement.InnerText = main.questUIText;
            quest.AppendChild(questUITextElement);

            if (main.MonologueID != null)
            {
                XmlElement monoElemnet = copyDoc.CreateElement("MonologueID");
                monoElemnet.InnerText = main.MonologueID;
                quest.AppendChild(monoElemnet);
            }
            if(main.NextSceneName != null)
            {
                XmlElement next = copyDoc.CreateElement("NextSceneName");
                next.InnerText = main.NextSceneName;
                quest.AppendChild(next);
            }

            XmlElement Option = copyDoc.CreateElement("Option");
            if (main.GetOffStateID != null)
                Option.SetAttribute("GetOffStateID", main.GetOffStateID);
            if (main.GetOnStateID != null)
                Option.SetAttribute("GetOnStateID", main.GetOnStateID);
            if (main.ChSpwan != null)
                Option.SetAttribute("ChSpwan", main.ChSpwan);
            quest.AppendChild(Option);

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            classNode.AppendChild(quest);
        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/QL.xml");
    }
    // 서브 퀘스트
    private void QTXml()
    {
        subList = new List<QuestSub>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("QT");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/QT.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("QT").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            QuestSub sub;

            sub.SceneName = node.Attributes["SceneName"].Value;
            sub.QuestGroupID = node.Attributes["QuestGroupID"].Value;
            sub.ID = node.Attributes["ID"].Value;
            sub.QuestScript = node.Attributes["QuestScript"].Value;
            sub.GetProvisoID = node.Attributes["GetProvisoID"].Value;
            subList.Add(sub);
        }

        //------------- 붙여넣기 ----------------//
        // 씬 별 노드 생성
        foreach (string sceneName in Enum.GetNames(typeof(Scene)))
        {
            XmlElement sceneElement = copyDoc.CreateElement(sceneName);

            root.AppendChild(sceneElement);
        }
        copyDoc.Save(Application.dataPath + "/Resources/QT.xml");
        foreach (QuestSub sub in subList)
        {

            // 해당 단서 클래스 노드 선택
            XmlNode classNode = copyDoc.SelectSingleNode("QT/" + sub.SceneName);

            // 메인 퀘스트별 노드 생성
            XmlElement title = copyDoc.CreateElement("Title");
            title.SetAttribute("QuestGroupID", sub.QuestGroupID);

            // 서브 퀘스트 노드 
            XmlElement nSub = copyDoc.CreateElement("Sub");
            nSub.SetAttribute("ID", sub.ID);
            nSub.SetAttribute("QuestScript", sub.QuestScript);
            nSub.SetAttribute("GetProvisoID", sub.GetProvisoID);
            title.AppendChild(nSub);

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            classNode.AppendChild(title);
        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/QT.xml");
    }

    private void EventXml()
    {
        eventList = new List<EventTable>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("Event");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/Event.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("Event").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            EventTable tEvent;

            tEvent.eventName = node.Attributes["EventName"].Value;
            tEvent.CameraName = node.Attributes["CameraName"].Value;
            tEvent.QuestListUnlock = node.Attributes["QuestListUnlock"].Value;
            tEvent.GetOnStateID = null;
            if (node.Attributes["GetOnStateID"] != null)
            tEvent.GetOnStateID = node.Attributes["GetOnStateID"].Value;

            eventList.Add(tEvent);
        }

        //------------- 붙여넣기 ----------------//
        foreach (EventTable table in eventList)
        {

            // 해당 단서 클래스 노드 선택
            XmlNode nRoot = copyDoc.SelectSingleNode("Event");

            // 이벤트 별 노드 생성
            XmlElement eventName = copyDoc.CreateElement(table.eventName);

            XmlElement cam = copyDoc.CreateElement("CameraName");
            cam.InnerText = table.CameraName;
            eventName.AppendChild(cam);

            XmlElement option = copyDoc.CreateElement("Option");
            option.SetAttribute("QuestListUnlock", table.QuestListUnlock);
            option.SetAttribute("GetOnStateID", table.GetOnStateID);
            eventName.AppendChild(option);

            nRoot.AppendChild(eventName);

        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/Event.xml");
    }

    enum Ch
    {
        빅토리아,
        루시앙,
        닥터,
        윌리,
        모도
    }
    private void DialXml()
    {
        selectList = new List<SelectTable>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("Select");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/Select.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("Select").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            SelectTable sel;

            sel.sceneName = node.Attributes["SceneName"].Value;
            sel.chName = node.Attributes["ChName"].Value;
            sel.id = node.Attributes["ID"].Value;
            sel.defaultState = node.Attributes.GetNamedItem("DefaultState").Value;
            sel.selectText = node.Attributes.GetNamedItem("SelectKrText").Value;
            sel.nextID = node.Attributes["ScriptNextID"].Value;

            sel.nextScene = null;
            if (node.Attributes["ScriptNextScene"] != null)
                sel.nextScene = node.Attributes["ScriptNextScene"].Value;

            selectList.Add(sel);
        }

        //------------- 붙여넣기 ----------------//
        // 씬 별로 노드 생성
        foreach (string sceneName in Enum.GetNames(typeof(Scene)))
        {
            XmlElement sceneElement = copyDoc.CreateElement(sceneName);
            foreach (string str_ch in Enum.GetNames(typeof(Ch)))
            {
                XmlNode ch = copyDoc.CreateElement(str_ch);
                sceneElement.AppendChild(ch);
            }

            root.AppendChild(sceneElement);
        }
        copyDoc.Save(Application.dataPath + "/Resources/Select.xml");
        foreach (SelectTable sel in selectList)
        {

            // 해당 단서 클래스 노드 선택
            XmlNode classNode = copyDoc.SelectSingleNode("Select/" + sel.sceneName+"/"+sel.chName);


            // 선택지 노드 
            XmlElement select = copyDoc.CreateElement("Sel");
            select.SetAttribute("ID", sel.id);
            select.SetAttribute("DefaultState", sel.defaultState);
            select.SetAttribute("SelectKrText", sel.selectText);
            select.SetAttribute("ScriptNextID", sel.nextID);

            if (sel.nextScene != null)
            {
                select.SetAttribute("ScriptNextScene", sel.nextScene);
            }

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            classNode.AppendChild(select);
        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/Select.xml");
    }

    private void CriminalXml()
    {
        List<CriminalTable> cList = new List<CriminalTable>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("Criminal");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/Criminal.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("Criminal").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            CriminalTable main;

            main.CriminalType = node.Attributes["CriminalType"].Value;
            main.ID = node.Attributes["ID"].Value;
            main.ScriptChName = node.Attributes["ScriptChName"].Value;
            main.ScriptChText = node.Attributes["ScriptChText"].Value;
            main.ScriptNextID = node.Attributes["ScriptNextID"].Value;

            main.MonoScriptID = null;
            if (node.Attributes["MonoScriptID"] != null)
                main.MonoScriptID = node.Attributes.GetNamedItem("MonoScriptID").Value;

            main.GroupID = null;
            if (node.Attributes["GroupID"] != null)
                main.GroupID = node.Attributes["GroupID"].Value;

            main.DownLife = null;
            if (node.Attributes["DownLife"] != null)
                main.DownLife = node.Attributes["DownLife"].Value;

            main.CamControl = null;
            if (node.Attributes["CameControl"] != null)
                main.CamControl = node.Attributes["CameControl"].Value;

            main.AniControl = null;
            if (node.Attributes["AniControl"] != null)
                main.AniControl = node.Attributes["AniControl"].Value;

            main.BGNameTrans = null;
            if (node.Attributes["BGNameTrans"] != null)
                main.AniControl = node.Attributes["BGNameTrans"].Value;

            main.ChSpwan = null;
            if (node.Attributes["ChSpwan"] != null)
                main.AniControl = node.Attributes["ChSpwan"].Value;

            cList.Add(main);
        }

        //------------- 붙여넣기 ----------------//

        // 씬 별로 노드 생성
        foreach (string criminal in Enum.GetNames(typeof(CriminalType)))
        {
            XmlElement cElement = copyDoc.CreateElement(criminal);

            root.AppendChild(cElement);
        }

        foreach (CriminalTable main in cList)
        {
            // 해당 단서 클래스 노드 선택
            XmlNode classNode = copyDoc.SelectSingleNode("Criminal/" + main.CriminalType);

            // 퀘스트별 노드 생성
            XmlElement CrimSel = copyDoc.CreateElement("CrimSel");
            CrimSel.SetAttribute("ID", main.ID);

            // 관련 노드 생성
            XmlElement name = copyDoc.CreateElement("ScriptChName");
            name.InnerText = main.ScriptChName;
            CrimSel.AppendChild(name);

            XmlElement text = copyDoc.CreateElement("ScriptChText");
            text.InnerText = main.ScriptChText;
            CrimSel.AppendChild(text);

            XmlElement next = copyDoc.CreateElement("ScriptNextID");
            next.InnerText = main.ScriptNextID;
            CrimSel.AppendChild(next);

            if (main.DownLife != null)
            {
                XmlElement down = copyDoc.CreateElement("DownLife");
                down.InnerText = main.DownLife;
                CrimSel.AppendChild(down);
            }
            if (main.GroupID!= null)
            {
                XmlElement group = copyDoc.CreateElement("GroupID");
                group.InnerText = main.GroupID;
                CrimSel.AppendChild(group);
            }
            XmlElement Option = copyDoc.CreateElement("Option");
            if (main.MonoScriptID != null)
                Option.SetAttribute("MonoScriptID", main.MonoScriptID);
            if (main.CamControl != null)
                Option.SetAttribute("CameControl", main.CamControl);
            if (main.BGNameTrans != null)
                Option.SetAttribute("BGNameTrans", main.BGNameTrans);
            if (main.AniControl != null)
                Option.SetAttribute("AniControl", main.AniControl);
            if (main.ChSpwan != null)
                Option.SetAttribute("ChSpwan", main.ChSpwan);
            CrimSel.AppendChild(Option);

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            classNode.AppendChild(CrimSel);
        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/Criminal.xml");
    }


    struct CrimGroup
    {
        public string GroupID;
        public string ScriptDesc;
        public string ScriptNextID;
    }
    private void CrimGroupXml()
    {
        List<CrimGroup> cList = new List<CrimGroup>();

        //------------- 변경 후 저장할 xml 파일 생성 ----------------//
        copyDoc = new XmlDocument();

        XmlDeclaration dec = copyDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
        XmlElement root = copyDoc.CreateElement("CrimGroup");

        copyDoc.AppendChild(dec);
        copyDoc.AppendChild(root);

        copyDoc.Save(Application.dataPath + "/Resources/CrimGroup.xml");

        //------------- 기본 xml 불러오기 ----------------//
        orignDoc = new XmlDocument();
        orignDoc.LoadXml(tableXml.text);

        //------------- 내용 복사 ----------------//
        XmlNodeList nodeList = orignDoc.SelectSingleNode("CrimGroup").ChildNodes;
        foreach (XmlNode node in nodeList)
        {
            CrimGroup main;

        
            main.GroupID = node.Attributes["GroupID"].Value;
            main.ScriptDesc = node.Attributes["ScriptDesc"].Value;
            main.ScriptNextID = node.Attributes["ScriptNextID"].Value;

            cList.Add(main);
        }

        //------------- 붙여넣기 ----------------//

        foreach (CrimGroup main in cList)
        {
            // 해당 단서 클래스 노드 선택
            XmlNode start = copyDoc.SelectSingleNode("CrimGroup");

            // 퀘스트별 노드 생성
            XmlElement Think = copyDoc.CreateElement("Think");
            Think.SetAttribute("GroupID", main.GroupID);

            // 관련 노드 생성
            XmlElement desc = copyDoc.CreateElement("ScriptDesc");
            desc.InnerText = main.ScriptDesc;
            Think.AppendChild(desc);

            XmlElement next = copyDoc.CreateElement("ScriptNextID");
            next.InnerText = main.ScriptNextID;
            Think.AppendChild(next);

            // 클래스 노드 아래 단서 이름 노드 붙이기 
            start.AppendChild(Think);
        }
        //저장
        copyDoc.Save(Application.dataPath + "/Resources/CrimGroup.xml");
    }
}
