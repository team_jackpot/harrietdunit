﻿Shader "jackpot/table" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

		_MainTex ("메인", 2D) = "white" {}
		_BumpMap ("메인노말", 2D) = "bump"{}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_Glossiness2 ("Smoothness2", Range(0,1)) = 1
		_Glossinessmap ("Smoothness", 2D) = "black" {}

		_Occlusion ("Occlusion", 2D) = "white" {}
		_OcclusionStrength ("AO 강도", float) =1

		_MainTex2 ("기둥", 2D) = "white" {}
		_BumpMap2 ("기둥", 2D) = "bump"{}

		_NM ("노말값 조절" , Float ) = 1
		
		_Occlusion2 ("Occlusion", 2D) = "white" {}
		
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
				
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MainTex2;
		sampler2D _Occlusion;
		sampler2D _Occlusion2;
		sampler2D _BumpMap ; 
		sampler2D _BumpMap2 ;
		sampler2D _Glossinessmap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
			float2 uv_BumpMap ;
			float2 uv_BumpMap2 ;
			float4 color:COLOR ;
			float2 uv_Glossinessmap;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _NM ; 
		float _Glossiness2;
		float _OcclusionStrength;
		

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 e = tex2D (_MainTex2, IN.uv_MainTex2) * _Color;

			float4 ao = tex2D (_Occlusion, IN.uv_MainTex);
			float4 ao2 = tex2D (_Occlusion2, IN.uv_MainTex2);

			float4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap) * _Color;

			float3 n = UnpackNormal (tex2D (_BumpMap, IN.uv_MainTex))*_NM;
			float3 n2 = UnpackNormal (tex2D (_BumpMap2, IN.uv_MainTex2));

			//float3 FN = (IN.color.r * n3.rg * _NM, n3.b );
			
			o.Normal = n;
			o.Normal = lerp (n , n2 , IN.color.r) ;

			//o.Normal = float3 (IN.color.r * n3.r * _NM, IN.color. * n3.g * _NM, n3.b ) ;
			
			o.Occlusion = pow(ao, _OcclusionStrength);
			o.Occlusion = lerp(ao, ao2, IN.color.r) ; 
			
			o.Albedo = lerp(c.rgb, e.rgb, IN.color.r) ;

			o.Metallic =  _Metallic;
			o.Smoothness = _Glossiness;
			o.Smoothness = ((1 - g) *_Glossiness2).rgb; 
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
