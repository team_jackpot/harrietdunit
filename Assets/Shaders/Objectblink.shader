﻿Shader "jackpot/custom/Objectblink" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Glossinessmap ("Smoothness", 2D) = "black" {}

		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MetallicGlossmap ("Metallic", 2D) = "black" {}
		_Bumpmap("Normal",2D)= "bump"{}
		_OcclusionMap ("AO", 2D) = "white" {}
		_OcclusionStrength ("AO*n", float) =1

		[Toggle] _Ifem ("단서 이펙트를 사용합니까?",float) = 0
		[HDR] _EmColor("Emission color", color) = (2,1.12,0,1)
		//_Blink("깜빡거리는 속도",float) = 0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		cull off
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows 
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Glossinessmap;
		sampler2D _MetallicGlossmap;		
		sampler2D _Bumpmap;
		sampler2D _OcclusionMap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Glossinessmap;
			float2 uv_MetallicGlossmap;
			float2 uv_Bumpmap;
			float2 uv_OcclusionMap;
		};

		float _Glossiness, _Metallic, _OcclusionStrength, _Ifem;
		//float _Blink;
		float4 _Color,_EmColor;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			float4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			float4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap);
			float4 m = tex2D (_MetallicGlossmap, IN.uv_MetallicGlossmap);
			float4 ao = tex2D (_OcclusionMap, IN.uv_OcclusionMap);

			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));

			
			o.Albedo = c.rgb;
			o.Metallic = (m*_Metallic).rgb;
			o.Smoothness = (g *_Glossiness).rgb; 
			o.Normal =  normalmap;
			o.Occlusion = pow(ao, _OcclusionStrength);
			
			o.Alpha = c.a;

			float3 emMap = (c.r + c.g + c.b)/3;
			float blink = saturate(emMap * (cos(_Time.y*5)*0.5+0.5)); //_Blink=5
			
			o.Emission = emMap*_EmColor*blink*_Ifem;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
