﻿Shader "Custom/molding" {
	Properties {
		
		_MainTex ("메인", 2D) = "white" {}
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
				
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
	

		struct Input {
			float2 uv_MainTex;
	
			float4 color:COLOR ;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) ;
			
			o.Albedo = IN.color.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
