﻿Shader "jackpot/custom/outline/floor" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Glossinessmap ("Smoothness", 2D) = "white" {}
		_Glossinessmap2 ("Smoothness2", 2D) = "white" {}


		_Metallic ("Metallic", Range(0,1)) = 0.0
		//_MetallicGlossmap ("Metallic", 2D) = "black" {}

		_Bumpmap("Normal",2D)= "bump"{}
		_Bumpmap2("Normal",2D)= "white"{}
		_OcclusionMap ("AO", 2D) = "white" {}
		_OcclusionStrength ("AO*n", float) =1

		_Stain ("핏자국", 2D) = "white" {}
		[Toggle] _Ifem ("단서 이펙트를 사용합니까?",float) = 0
		[HDR] _EmColor("Emission color", color) = (2,1.12,0,1)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		cull back
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Glossinessmap, _Glossinessmap2;
//		sampler2D _MetallicGlossmap;		
		sampler2D _Bumpmap;
		sampler2D _OcclusionMap;
		sampler2D _Stain;




		struct Input {
			float2 uv_MainTex;
			float2 uv_Glossinessmap, uv_Glossinessmap2;
	//		float2 uv_MetallicGlossmap;
			float2 uv_Bumpmap;
			float2 uv_OcclusionMap;
			float2 uv_Stain;
		};

		half _Glossiness, _OcclusionStrength;
		half _Metallic, _Ifem;
		fixed4 _Color, _EmColor;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 stained = tex2D (_Stain, IN.uv_Stain);
			fixed4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap);
			fixed4 g2 = tex2D (_Glossinessmap2, IN.uv_Glossinessmap2);

			fixed4 ao = tex2D (_OcclusionMap, IN.uv_OcclusionMap);

			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));

			float3 emMap =(stained.r + stained.g + stained.b)/3;
									
			o.Albedo = (c*stained).rgb;
			
			
			o.Metallic = _Metallic;
			o.Smoothness = lerp((g*g2 *_Glossiness).r, 0.75, 1-stained.r); 
			o.Alpha = c.a;
			o.Normal =  normalmap;
			o.Occlusion = ao*_OcclusionStrength;

		}
		ENDCG






		cull front
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert 

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Bumpmap2;
		sampler2D _Stain;

	
		struct Input {
			float2 uv_Stain;
			float2 uv_MainTex;
			float2 uv_Bumpmap2;

			
		};

		half  _Ifem;
		fixed4 _Color, _EmColor;

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			float3 normalmap2 = tex2D(_Bumpmap2, IN.uv_Bumpmap2);
			//o.Normal =  normalmap2;

		

			o.Emission = _EmColor.rgb;
			o.Alpha = 1;
		}

		void vert(inout appdata_full v){
			
			v.vertex.xyz += v.normal*_Ifem *(cos(_Time.y*5)*0.5+0.5)*0.008;

		}

		ENDCG



	}
	FallBack "Diffuse"
}
