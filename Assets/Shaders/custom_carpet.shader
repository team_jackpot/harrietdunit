﻿Shader "jackpot/custom/carpet" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MainTex2 ("Pattern", 2D) = "white" {}
		_Lerppower("Lerppower",range(0,1)) = 0.5

		_MaskTex ("R:Metallic / G:smoothness / B: alpha", 2D) = "black" {}
		_Glossiness ("Smoothness", Range(0,2)) = 1
		_Metallic ("Metallic", Range(0,1)) = 1
		_Bumpmap("Normal",2D)= "bump"{}
		_Bumpmap2("Normal - Detailmap",2D)= "bump"{}
		_Bumppower("전체범프세기",range(0,10)) = 3

		_Bumppower2("bumppower",range(0,10)) = 3
		_Bumppower3("디테일",range(0,10)) = 1

		_Cutoff("cutout",float) = 0.5

		_Stain ("핏자국", 2D) = "white" {}

		[Toggle] _Ifem ("단서 이펙트를 사용합니까?",float) = 0
		[HDR] _EmColor("Emission color", color) = (0,1.5,2,1)


	}
	SubShader {
		Tags { "RenderType"="AlphaTest" " Queue" = "  TransparentCutout " }
		LOD 200
		cull off
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alphatest:_Cutoff

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MainTex2;
		sampler2D _MaskTex;
		sampler2D _Bumpmap;
		sampler2D _Bumpmap2;
		sampler2D _Stain;


		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
			float2 uv_MaskTex;
			float2 uv_Bumpmap;
			float2 uv_Bumpmap2;
			float2 uv_Stain;

		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color, _EmColor;
		float _Bumppower;
		float _Bumppower2, _Bumppower3;
		float _Lerppower, _Ifem;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			
			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));
			float3 normalmap2 = UnpackNormal(tex2D(_Bumpmap2,IN.uv_Bumpmap2));

			float3 fimalnormal = (normalmap*_Bumppower2 + normalmap2*_Bumppower3)/2;

			fixed4 stained = tex2D (_Stain, IN.uv_Stain);

			
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 d = tex2D (_MainTex2, IN.uv_MainTex2);
			fixed4 m = tex2D (_MaskTex, IN.uv_MaskTex);
			//o.Albedo = (lerp(c,d,_Lerppower) * stained) .rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = saturate( m.r * _Metallic);
			o.Smoothness = lerp(saturate(m.g * _Glossiness), 0.8, 1-stained.r);

			float3 emMap = (stained.r + stained.g + stained.b)/3;
			float blink = saturate(emMap * (cos(_Time.y*5)*0.5+0.5)); //_Blink=5
			
			//o.Emission = (1-emMap)*_EmColor*blink*_Ifem;
			o.Albedo = ((lerp(c,d,_Lerppower) * stained) +(1-emMap)*_EmColor*blink*_Ifem).rgb;


			o.Normal = float3(fimalnormal.rg * _Bumppower ,fimalnormal.b) ;
			o.Alpha = m.b;
		}
		ENDCG
	}
	Fallback "Legacy Shaders/Transparent/Cutout/VertexLit"
}
