﻿Shader "jackpot/custom/outside_buildings" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("vertexC = K", 2D) = "white" {}
		_MainTex2("vertexC = R", 2D) = "white" {}
		_MainTex3("vertexC =G", 2D) = "white" {}
		_MainTex4("vertexC =B", 2D) = "white" {}

		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		cull off
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;		
		sampler2D _MainTex2;
		sampler2D _MainTex3;
		sampler2D _MainTex4;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
			float2 uv_MainTex3;
			float2 uv_MainTex4;
			float3 color : Color;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = 1- tex2D (_MainTex, IN.uv_MainTex);
			fixed4 d = tex2D (_MainTex2, IN.uv_MainTex2);
			fixed4 e = tex2D (_MainTex3, IN.uv_MainTex3);
			fixed4 f = tex2D (_MainTex4, IN.uv_MainTex4);
			o.Albedo = lerp(c*0.5, d, IN.color.r).rgb;
			o.Albedo = lerp(o.Albedo, e, IN.color.g).rgb;
			o.Albedo = lerp(o.Albedo, f, IN.color.b).rgb * _Color;

			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
