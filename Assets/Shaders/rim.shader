﻿Shader "Custom/rim" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_RimColor ("RimColor", Color ) = (1,1,1,1)
		_RimPower ("RimPower", float) = 2
	}
	SubShader {
		Tags { "RenderType"="Tansparent" "Queue" = "Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha:fade
		#pragma target 3.0

		sampler2D _MainTex;
		float4 _RimColor ; 
		float _RimPower ; 

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
			float3 worldNormal;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			float rim = dot(IN.worldNormal, IN.viewDir);
			// o.Emission = pow(1-rim, _RimPower) * _RimColor.rgb; 
			o.Metallic = _Metallic;
			 //o.Smoothness = min(pow(1-rim, _RimPower),0.91) ; 
			 o.Smoothness = min(pow(1-rim, _RimPower)*2,0.95) ; 
			// o.Smoothness = 0.9;
			o.Alpha =c.a; 
			
			//o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
