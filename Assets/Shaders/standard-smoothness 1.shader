﻿Shader "jackpot/standard-smoothness1" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Glossinessmap ("Smoothness", 2D) = "black" {}

		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Metallicmap ("Metallic", 2D) = "black" {}

		_Bumpmap("Normal",2D)= "bump"{}
		_AOmap ("AO", 2D) = "white" {}
		_Rimcolor("Color", Color) = (1,1,1,1)



	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		//cull off
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Glossinessmap;
		sampler2D _Metallicmap;		
		sampler2D _Bumpmap;
		sampler2D _AOmap;




		struct Input {
			float2 uv_MainTex;
			float2 uv_Glossinessmap;
			float2 uv_Metallicmap;
			float2 uv_Bumpmap;
			float2 uv_AOmap;
			float3 viewDir;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color,_Rimcolor;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap);
			fixed4 m = tex2D (_Metallicmap, IN.uv_Metallicmap);
			fixed4 ao = tex2D (_AOmap, IN.uv_AOmap);

			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap));

			float rim = dot(IN.viewDir,normalmap);
	//		o.Emission =saturate( pow(rim,80)) * float3(0.5,0.2,0.1) * 0.2;
			o.Emission =saturate( pow((1-rim),10)) * _Rimcolor.rgb * 0.2;


			o.Albedo = c.rgb;
			o.Metallic = (m*_Metallic).rgb;
			o.Smoothness = (g *_Glossiness).rgb; 
			o.Alpha = c.a;
			o.Normal =  normalmap;
			o.Occlusion = ao;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
