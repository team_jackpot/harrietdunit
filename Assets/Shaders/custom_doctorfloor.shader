﻿Shader "jackpot/custom/doctorfloor" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5

		 _Glossinessmap ("Smoothness", 2D) = "black" {}
		_Glossinessmap2 ("Smoothness", 2D) = "white" {}

		_Glossiness22("랜덤스무스니스맵 강도 +", range(0,1)) = 1
		_Glossiness23("랜덤스무스니스맵 강도 *", range(0,1)) = 1
		_Glossinessmap3 ("Smoothness", 2D) = "white" {}



		_Bumpmap("Normal",2D)= "bump"{}
		_Bumpmap2("Normal",2D)= "bump"{}
		_NormalPower ("detail*n", float) =1
		//_OcclusionMap ("AO", 2D) = "white" {}
		_OcclusionStrength ("AO*n", float) =1
		



	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		//cull off
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Glossinessmap;
		sampler2D _Glossinessmap2;
		sampler2D _Glossinessmap3;
		sampler2D _MetallicGlossmap;		
		sampler2D _Bumpmap;
		sampler2D _Bumpmap2;
		//sampler2D _OcclusionMap;




		struct Input {
			float2 uv_MainTex;
			float2 uv_Glossinessmap;
			float2 uv_Glossinessmap2;
			float2 uv_Glossinessmap3;
			float2 uv_MetallicGlossmap;
			float2 uv_Bumpmap;
			float2 uv_Bumpmap2;
		//	float2 uv_OcclusionMap;
			

		};

		float _Glossiness, _NormalPower, _Glossiness22, _Glossiness23;
		float _Metallic, _OcclusionStrength;
		float4 _Color;


		void surf (Input IN, inout SurfaceOutputStandard o) {
			float4 c = tex2D (_MainTex, IN.uv_MainTex);
			float4 g = tex2D (_Glossinessmap, IN.uv_Glossinessmap);
			float4 g3 =saturate( tex2D (_Glossinessmap3, IN.uv_Glossinessmap3)+0.6);
			float4 g2 = saturate(tex2D (_Glossinessmap2, IN.uv_Glossinessmap2) *_Glossiness23 +_Glossiness22);
			// float4 ao = tex2D (_OcclusionMap, IN.uv_OcclusionMap);

			float3 normalmap = UnpackNormal(tex2D(_Bumpmap, IN.uv_Bumpmap)) * _NormalPower;
			float3 normalmap2 = UnpackNormal(tex2D(_Bumpmap2, IN.uv_Bumpmap2))  ;

			


			
			o.Albedo = g.rgb* _Color.rgb;
			o.Metallic = 0;
			o.Smoothness = (c.g * g2 * g3*_Glossiness).rgb; 
			o.Alpha = c.a;
			o.Normal =  (normalmap+normalmap2)/2;
			o.Occlusion = pow(c.b, _OcclusionStrength);

		}
		ENDCG
	}
	FallBack "Diffuse"
}
