﻿Shader "jackpot/custom/filament" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		[HDR] _EmissionColor ("Color", Color) = (1,1,1,1)



	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		cull off
		
		CGPROGRAM
		#pragma surface surf Lambert noambient vertex:vert 

		#pragma target 3.0

		struct Input {
			float2 uv_MainTex;
			

		};

		float4 _Color, _EmissionColor;

		void vert(inout appdata_full v){
			v.vertex.xyz =v.vertex.xyz+ v.normal* 0.001;
		}


		void surf (Input IN, inout SurfaceOutput o) {
			
			o.Albedo = 0;
			o.Alpha =1;
			o.Emission = _EmissionColor;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
