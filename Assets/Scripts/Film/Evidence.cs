﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum EviState { Default, Over, Click }
public class Evidence : MonoBehaviour {
    string mName;
    EviState state;
    GameObject obj;

    void Start () {
        name = gameObject.name;
        state = EviState.Default;
        obj = transform.GetChild(0).gameObject;
        obj.SetActive(false);
    }
    public void setState(Color c)
    {
        GetComponent<Image>().color = c;
    }
    public void EviPopupActive(bool b)
    {
        obj.SetActive(b);
    }
}
