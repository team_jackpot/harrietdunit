﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ExpandFilm : MonoBehaviour {
    GameObject[] film;
    GameObject films;
    RawImage evidenceArea;
    Image sFilm;
    int count;
    void Start () {
        films = GameObject.Find("Films");
        sFilm = GameObject.Find("SmallFilm").GetComponent<Image>();
        count = films.transform.childCount;
        Init();
        evidenceArea = transform.GetComponent<RawImage>();
        //evidenceArea.texture = Resources.Load("FilmUI/UI_smallfilm_add") as Texture;
    }
	void Update () {
        Check();
	}
    void Check()
    {
        float dis;
        for (int i = 0; i < count; i++){
            dis = Vector3.Distance(sFilm.rectTransform.transform.position,
                                        film[i].GetComponent<RawImage>().rectTransform.transform.position);
            //Debug.Log("filmDisrance = " + dis);
            if (dis < 10.0f){
                evidenceArea.texture = film[i].GetComponent<RawImage>().texture;
            }
        }
    }
    void Init()
    {
        film = new GameObject[count];
        for(int i = 0; i< count; i++)
        {
            film[i] = films.transform.GetChild(i).gameObject;
        }
    }
}
