﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class calc
{
    public int add(int a,int b) { return a + b; }
    public int sub(int a,int b) { return a - b; }
}
public class PointerManager : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler,IPointerExitHandler{
    EventTrigger.Entry mEntry;
    EventTrigger eTrigger;
    bool isActive;
    Image img;

	void Start () {
        mEntry = new EventTrigger.Entry();
        mEntry.eventID = EventTriggerType.PointerUp;
        eTrigger = GetComponent<EventTrigger>();
        isActive = false;
    }
    
    public void OnPointerDown(PointerEventData data)
    {
        GetComponent<Evidence>().setState(Color.red);
        isActive = false;
        GetComponent<Evidence>().EviPopupActive(isActive);
        Debug.Log("Pointer Down~~~~~~~");
    }
    public void OnPointerUp(PointerEventData data)
    {
        GetComponent<Evidence>().setState(Color.blue);
        isActive = false;
        GetComponent<Evidence>().EviPopupActive(isActive);
        Debug.Log("Pointer Up~~~~");
    }
    public void OnPointerEnter(PointerEventData data)
    {
        GetComponent<Evidence>().setState(Color.white);
        isActive = true;
        GetComponent<Evidence>().EviPopupActive(isActive);
        Debug.Log("Pointer Enter~~~~~~~");
    }

    public void OnPointerExit(PointerEventData data)
    {
        GetComponent<Evidence>().setState(Color.gray);
        isActive = false;
        GetComponent<Evidence>().EviPopupActive(isActive);
        Debug.Log("Pointer Exit~~~~~~~");
    }
}
