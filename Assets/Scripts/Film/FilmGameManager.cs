﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Xml;
public class mNode
{
    public string Id = string.Empty;
    public string ScriptType = string.Empty;
    public string ScriptChName = string.Empty;
    public string ScriptChText = string.Empty;
    public string FilmMove = string.Empty;
    public string DeleteBlackArt = string.Empty;
    public string EvidenceTrueCheck = string.Empty;
    public string Evidence1st = string.Empty;
    public string Evidence2st = string.Empty;
    public string Evidence3st = string.Empty;
    public string Evidence4st = string.Empty;
    public string TrueScriptID = string.Empty;
    public string FalseScriptID = string.Empty;
}
public enum State { Text, Film}
public class FilmGameManager : MonoBehaviour {
    State state;
    string TrueScriptID,FalseScriptID;
    float FilmSpeed; //필름이 이동할 속도
    float FilmRange; //필름이 이동할 방향
    float FilmDistance; //필름이 이동할 거리
    float FilmAreaDis; //필름 증거인식 거리
    //증거 드랍 인식 UI
    RawImage rImgFilm, FilmGameArea,rImgFilms;
    Image Evidence, smallFilm;
    Text chName,chText;
    Canvas mCanvas;
    GraphicRaycaster gr;
    PointerEventData ped;
    bool mDrag, filmArea;
    mNode[] node;
    string st;
    int count;
    GameObject selectObj;
    string trueEvidence;
    public string TrueEvidence {get{ return trueEvidence; }set { this.trueEvidence = value; } }
    public bool FilmArea { set { this.filmArea = value; } }

    bool _isNext = false;

    static FilmGameManager _instance;
    public static FilmGameManager Instance()
    {
        if (_instance == null)
        {
            _instance = GameObject.FindObjectOfType<FilmGameManager>();
        }
        return _instance;
    }
    int i = 0;//node[i]
    void Start () {
        LoadXML();
        state = State.Text;
        FilmSpeed = 1.0f;
        FilmRange = 0.0f;
        FilmDistance = 1.0f;
        FilmAreaDis = 200.0f;
        rImgFilm = GameObject.Find("Film").GetComponent<RawImage>();
        rImgFilms = GameObject.Find("Films").GetComponent<RawImage>();
        FilmGameArea = GameObject.Find("FilmEvdenceArea").GetComponent<RawImage>();
        mCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        chName = GameObject.Find("ScriptChName").GetComponent<Text>();
        chText = GameObject.Find("ScriptChText").GetComponent<Text>();
        gr = mCanvas.GetComponent<GraphicRaycaster>();
        ped = new PointerEventData(null);
        mDrag = true;filmArea = false;

        LogScript(node[i].ScriptChName, node[i].ScriptChText);//이름, 대사출력
        i++;
    }

	void Update () {
        switch (state){
            case (State.Text):
                if (Input.GetMouseButtonDown(0)||Input.GetButtonDown("Jump")){
                    if (i < count){
                        _isNext = true;
                        LogScript(node[i].ScriptChName, node[i].ScriptChText);//이름, 대사출력
                        if (node[i].ScriptType == "Film") state = State.Film;//필름일경우 상태변경
                        if (node[i].FilmMove == "Move") { StartCoroutine(FilmMove()); }//필름 무브
                        i++;
                        if ("3035" == node[i - 1].Id) GameObject.Find("Film_7").GetComponent<RawImage>().texture = GameObject.Find("Film_8").GetComponent<RawImage>().texture;
                    }
                }
                break;
            case (State.Film):
                EvidenceMove(); //타입이 필름일때 증거ui 조작가능
                break;
        }
    }
    Vector3 GetP()
    {
        return new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
    }
    Vector2 fPos;
    void EvidenceMove()
    {
        float Dis=100000.0f;
        if (Input.GetMouseButtonDown(0) && mDrag) {
            Evidence = getObj();
            if(Evidence) fPos = Evidence.rectTransform.transform.position;
        }else if (Input.GetMouseButton(0) && !mDrag){
            Dis = Vector3.Distance(selectObj.GetComponent<Image>().rectTransform.position, FilmGameArea.rectTransform.position);
            //Debug.Log("DevidenceDistance = " + Dis);
            ObjMove(GetP(), Evidence);
        }else if (Input.GetMouseButtonUp(0) && Evidence)
        {
            if (Evidence) { Dis = Vector3.Distance(selectObj.GetComponent<Image>().rectTransform.position, FilmGameArea.rectTransform.position);}
            mDrag = true;
            //단서가 맞는지 체크
            if (selectObj.name == node[i-1].EvidenceTrueCheck && Dis < FilmAreaDis){
                if ("UI_FilmEvidence_Shoes01" == node[i - 1].EvidenceTrueCheck) GameObject.Find("Film_2").GetComponent<RawImage>().texture = GameObject.Find("Film_3").GetComponent<RawImage>().texture;
                i = scriptSeach(node[i-1].TrueScriptID);
                state = State.Text;
                LogScript(node[i].ScriptChName, node[i].ScriptChText);//이름, 대사출력
                Destroy(selectObj.gameObject);
                i++;
            }else{
                i = scriptSeach(node[i-1].FalseScriptID);
                Evidence.rectTransform.transform.position = fPos;
                LogScript(node[i].ScriptChName, node[i].ScriptChText);//이름, 대사출력
            }
            filmArea = false;
        }
    }
    Image getObj()
    {
        ped.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>(); // 여기에 히트 된 개체 저장 
        gr.Raycast(ped, results);
        if (results.Count==0)
        {
            return null;
        }
        GameObject obj = results[0].gameObject;
        if (obj.CompareTag("Evidence")) // 히트 된 오브젝트의 태그와 맞으면 실행 
        {
            selectObj = obj.gameObject;
            Debug.Log("SelectName = " + selectObj.name);
            //Debug.Log("SelectName2 = " + results[1].gameObject.name);
            mDrag = false;
            return obj.GetComponent<Image>();
        }else { Debug.Log("getObj = NULL~~~~"); }
        return null;
    }
    int scriptSeach(string s)
    {
        int n=0;
        for(int i = 0; i < count; i++)
        {
            if (node[i].Id == s){
                n = i;
                break;
            }
        }
        return n;
    }
    IEnumerator AlphaDown(GameObject img)
    {
        Color col = new Color();
        col.a = 255;
        col.r = 255; col.g = 255; col.b = 255;
        while (img.GetComponent<RawImage>().color.a > 0) {
            col.a = col.a - 2;
            img.GetComponent<RawImage>().color = col;
            yield return new WaitForSeconds(0.05f);
        }
    }
    IEnumerator FilmMove(){
        for (float i = 0; i <= FilmDistance;){
            Rect r = rImgFilm.uvRect;
            Vector3 pos = new Vector3();
            pos.y = 16.2f * Time.deltaTime;
            r.y -= (FilmDistance)* Time.deltaTime ;
            i += (FilmDistance) * Time.deltaTime;
            rImgFilms.rectTransform.transform.position += pos;
            rImgFilm.uvRect = r;
            yield return new WaitForEndOfFrame();
        }
    }
    void LogScript(string name,string _text)
    {
        if (_isNext)
        {
            StopCoroutine(textPrint(chText, _text));
            _isNext = false;
        }
        chName.text = name;
        StartCoroutine(textPrint(chText,_text));
    }
    void ObjMove(Vector3 d,Image i)
    {
        i.rectTransform.transform.position = d;
    }
    IEnumerator filmTwinkle()
    {
        yield return new WaitForSeconds(0.1f);
    }
    IEnumerator textPrint(Text mText, string s)
    {
        int count = s.Length;
        Debug.Log("c = "+count);
        int num = 0;
        string txt = s[num].ToString();
        chText.text = "";
       while(!_isNext)
        {
            chText.text = txt;
            if (num < count) num++;
            if (num >= count) {
                _isNext = true;
                break;
            }
            //Debug.Log(num);
            txt += s[num].ToString();
            //Debug.Log(txt);

            yield return new WaitForSeconds(0.02f);
        }
        yield break;
       /* foreach (char ss in s)
        {
            chText.text += ss.ToString();
            yield return new WaitForSeconds(0.03f);
        }*/
    }
    void LoadXML()
    {
        Debug.Log("Load XmlNode~");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(Application.dataPath + "/Resources/xml2.xml");
        //하나씩 가져올때
        XmlNodeList nodeTable = xmlDoc.GetElementsByTagName("field");
        node = new mNode[nodeTable.Count];
        count = nodeTable.Count;
        int c = 0;
        for (int i = 0; i < nodeTable.Count; i++)
            node[i] = new mNode();
        foreach (XmlNode _node in nodeTable)
        {
            //if (_node.Attributes.GetNamedItem("Id") != null) { Debug.Log("No?NoT???"); break; }
            node[c].Id = (_node.Attributes.GetNamedItem("Id").Value);
            //Debug.Log(_node.Attributes.GetNamedItem("Id").Value);
            node[c].ScriptType = (_node.Attributes.GetNamedItem("ScriptType").Value);
            node[c].ScriptChName = (_node.Attributes.GetNamedItem("ScriptChName").Value);
            node[c].ScriptChText = (_node.Attributes.GetNamedItem("ScriptChText").Value);
            if (_node.Attributes.GetNamedItem("FilmMove") != null)
                node[c].FilmMove = (_node.Attributes.GetNamedItem("FilmMove").Value);
            if (_node.Attributes.GetNamedItem("DeleteBlackArt") != null)
                node[c].DeleteBlackArt = (_node.Attributes.GetNamedItem("DeleteBlackArt").Value);
            if (_node.Attributes.GetNamedItem("EvidenceTrueCheck") != null)
                node[c].EvidenceTrueCheck = (_node.Attributes.GetNamedItem("EvidenceTrueCheck").Value);
            if (_node.Attributes.GetNamedItem("Evidence1st") != null)
                node[c].Evidence1st = (_node.Attributes.GetNamedItem("Evidence1st").Value);
            if (_node.Attributes.GetNamedItem("Evidence2st") != null)
                node[c].Evidence2st = (_node.Attributes.GetNamedItem("Evidence2st").Value);
            if (_node.Attributes.GetNamedItem("Evidence3st") != null)
                node[c].Evidence3st = (_node.Attributes.GetNamedItem("Evidence3st").Value);
            if (_node.Attributes.GetNamedItem("Evidence4st") != null)
                node[c].Evidence4st = (_node.Attributes.GetNamedItem("Evidence4st").Value);
            if (_node.Attributes.GetNamedItem("TrueScriptID") != null)
                node[c].TrueScriptID = (_node.Attributes.GetNamedItem("TrueScriptID").Value);
            if (_node.Attributes.GetNamedItem("FalseScriptID") != null)
                node[c].FalseScriptID = (_node.Attributes.GetNamedItem("FalseScriptID").Value);
            c++;
        }
    }
    void logTest(XmlNodeList nt){ 
        foreach (XmlNode node in nt){
            Debug.Log(node.Attributes.GetNamedItem("Id").Value);
            Debug.Log(node.Attributes.GetNamedItem("ScriptType").Value);
            Debug.Log(node.Attributes.GetNamedItem("ScriptChName").Value);
            Debug.Log(node.Attributes.GetNamedItem("ScriptChText").Value);
        }
    }
}
