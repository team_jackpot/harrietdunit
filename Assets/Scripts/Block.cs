﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
    [SerializeField]
    private int number;
    public int Number { get { return number; } }
    [SerializeField]
    private bool on;
    public bool On { get { return on; } }

    private void Awake(){
        on = false;
    }
    void Start()
    {
    }
    public void StateChange()
    {
        if (on) {
            on = false;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
        }else {
            on = true;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.cyan;
        }
    }

    public void setInit(int n)
    {
        number = n;
    }
}
