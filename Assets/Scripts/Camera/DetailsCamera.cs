﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailsCamera : MonoBehaviour
{

    GameObject _subCamera;
    Camera _sub;
    Camera _camera;

    CameraCtrl _cameraCtrl;
    EventCamera _event;
    
    GameObject light;

    int itemLayer = (1 << 29) + (1 << 27);
    private Vector3 startPos, endPos;   // 마우스 시작점, 끝 점
    Vector3 angle;

    GameObject newObj;
    bool bDetailsCam = false;

    GameObject ui;
    Text txt_name;

    public void EndDetails()
    {
        bDetailsCam = false;

        _subCamera.SetActive(false);
        _camera.cullingMask = -1;
        _camera.clearFlags = CameraClearFlags.Skybox;

        Destroy(newObj);

        if (_cameraCtrl != null)
            _cameraCtrl.enabled = true;
        if (light != null)
            light.SetActive(false);

        GameManager.Instance.IsObserve = false;

        if (_event != null)
        {
            _event.enabled = true;
        }
        else
        {
            GameManager.Instance.PlayerEnable(true);
            if (UIManager.Instance.StartGame)
                UIManager.Instance.PlayMiniGame();
            else
            {
                UIManager.Instance.Quest.SetObserve(false);
                GameManager.Instance.IsObserve = false;
            }
        }
        this.enabled = false;
    }

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _cameraCtrl = GetComponent<CameraCtrl>();
        _event = GetComponent<EventCamera>();

        _subCamera = transform.GetChild(0).gameObject;
        _sub = _subCamera.GetComponent<Camera>();
        _subCamera.SetActive(false);

        ui = UIManager.Instance.UI_ob;
        txt_name = ui.GetComponentInChildren<Text>();

        if (transform.childCount >1)
            light = transform.GetChild(1).gameObject;
    }

    public void DetailsObserve(string _name)
    {
        if(_cameraCtrl != null)
            _cameraCtrl.enabled = false;
        if(_event != null)
            _event.enabled = false;
        if (light != null)
            light.SetActive(true);

        UIManager.Instance.Quest.SetObserve(true);
        GameManager.Instance.PlayerEnable(false);
        GameManager.Instance.IsObserve = true;

        bDetailsCam = true;
        _name = _name.Replace(" ", "");
        GameObject create = (GameObject)Resources.Load("Prefabs/Details/" + _name);

        _camera.clearFlags = CameraClearFlags.Depth;
        _camera.cullingMask = (1 << 28) ;

        _subCamera.SetActive(true);
        // 조사 오브젝트와 같은 이름 오브젝트 생성
       newObj= GameObject.Instantiate(create, transform.position + (transform.forward), create.transform.rotation);

    }


    void Update()
    {
        if (!DialoguePanel.Instance.IsTalk && bDetailsCam && !UIManager.Instance.OpenNote)
        {
            Click();

            if (Input.GetMouseButtonDown(1))
                MouseDown();
            if (Input.GetMouseButton(1))
                MouseMove();
        }
        else
        {
            ui.SetActive(false);
            startPos = Vector3.zero;
        }
    }



    // 마우스 오른쪽 버튼 다운
    void MouseDown()
    {
      //  angle = Vector3.zero;
        // 마우스 시작 위치 설정
        startPos = endPos = Input.mousePosition;
    }

    // 마우스 오른쪽 버튼 다운 + 드래그
    void MouseMove()
    {
        //현재 위치 설정
        endPos = Input.mousePosition;

        // 마우스 이동 x,y 값
        float angleX = endPos.x - startPos.x;
        float angleY = endPos.y - startPos.y;

        angle.x = -angleY;
        angle.y = -angleX;

        // 쿼터니언으로 변경
        Quaternion rot = Quaternion.Euler(angle);

        // 회전 값 설정
        newObj.transform.rotation = rot;
        // newObj.transform.Rotate(Vector3.right,angle.x*Time.deltaTime,Space.World);
        //newObj.transform.Rotate(Vector3.up, angle.y * Time.deltaTime, Space.World);
    }

    void Click()
    {
        Ray r = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, 0.95f, itemLayer))
        {
            GameObject h = hit.collider.gameObject;
            string name = h.name;
            if (h.layer == 29 && h.CompareTag("Untagged"))
            {
                ui.SetActive(true);
                txt_name.text = name;
                SetFUI(Input.mousePosition);
                UIManager.Instance.SetObUI("DET");

                if (Input.GetMouseButtonDown(0))
                {
                    name = name.Replace(" ", "");

                    //대사
                    UIManager.Instance.Dialoue.SetActive(true);
                    DialoguePanel.Instance.StartDialogue(DialogueType.DET, name);
                }
            }
        }
        else ui.SetActive(false);
    }

    void SetFUI(Vector2 objPos)
    {
        Vector2 screenPos = objPos + new Vector2(80, -10); // _camera.WorldToScreenPoint(objPos);

        Vector2 viewPos = _camera.ScreenToViewportPoint(screenPos);
        if (viewPos.x > 0.8f)
            viewPos.x = 0.8f;
        if (viewPos.x < 0.2f)
            viewPos.x = 0.2f;

        if (viewPos.y > 0.8f)
            viewPos.y = 0.8f;
        if (viewPos.y < 0.2f)
            viewPos.y = 0.2f;

        screenPos = _camera.ViewportToScreenPoint(viewPos);

        ui.transform.position = screenPos;
    }
}
