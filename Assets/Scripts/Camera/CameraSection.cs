﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraSection : MonoBehaviour {

    [SerializeField]
    private GameObject _camera;

    [SerializeField]
    private PostProcessingProfile profile;

    float ChkTime = 1.0f;
    float time;
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            time += Time.deltaTime;
            if (time >= ChkTime)
            {
                GameObject cam = GameManager.Instance.MainVirtual;
                if (cam != _camera)
                {
                    _camera.gameObject.SetActive(true);
                    cam.SetActive(false);

                    if (profile != null)
                    {
                        GameManager.Instance.Main.GetComponent<PostProcessingBehaviour>().profile = profile;
                    }

                    GameManager.Instance.MainVirtual = _camera;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            time = 0.0f;
        }
    }
}
