﻿//------------------------------------------------------------------//
// 작성자 : 조성혜
// 미니게임, 확대조사등의 이벤트 매니저 
// 최종 수정일자 : 18.06.17
//------------------------------------------------------------------//
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.SceneManagement;
using Cinemachine;
using UnityEngine.UI;
using System.Collections;
using System;

struct EventInfo
{
    public string eventName;
    public string cameraName;
    public string unlock;
    public string[] getOnStateID;
}
public class EventManager : MonoBehaviour {

    GameObject _mainCamera;
    GameObject[] _eventCamera;
    List<EventInfo> eventList;

    GameObject[] actionCamera;
    GameObject[] spwan;
    XmlDocument eventDoc;
    TextAsset eventText;

    XmlDocument objDoc;


	void Awake ()
    {
        eventText = (TextAsset)Resources.Load("Xml/Event");
        
        Init();
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    bool _spwan = false;
    public bool Spwan { set { _spwan = value; } }
    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        SceneType cur = GameManager.Instance.CurScene;
         
        if (cur != SceneType.Title)
            GameManager.Instance.Main = Camera.main.gameObject;
        GameManager.Instance.MainVirtual = GameObject.Find("V_Start");


        if (cur == SceneType.P_Room && _spwan)
        {
            GameObject s = GameObject.Find("State");
            GameObject on = s.transform.GetChild(0).gameObject;
            GameObject off = s.transform.GetChild(1).gameObject;
            on.SetActive(true);
            off.SetActive(false);
        }

        //-----------------------이벤트 카메라-----------------------------
        EventCamera[] _event = GameObject.FindObjectsOfType<EventCamera>();

        _eventCamera = new GameObject[_event.Length];
        for (int i = 0; i < _eventCamera.Length; i++)
        {
            _eventCamera[i] = _event[i].gameObject;
            _eventCamera[i].SetActive(false);
        }

        //-----------------------액션 카메라-------------------------------
        actionCamera = GameObject.FindGameObjectsWithTag("Action");

        for(int i=0; i<actionCamera.Length; i++)
        {
            actionCamera[i].SetActive(false);
        }
        //--------------------------스폰-------------------------------
        spwan = GameObject.FindGameObjectsWithTag("Spwan");
        for (int i = 0; i < spwan.Length; i++)
        {
            spwan[i].SetActive(false);
        }
    }

    private void Init()
    {
        eventList = new List<EventInfo>();
        eventDoc = new XmlDocument();
        eventDoc.LoadXml(eventText.text);

        XmlNode root = eventDoc.SelectSingleNode("Event");
        foreach(XmlNode node in root.ChildNodes)
        {
            EventInfo eInfo = new EventInfo();
            eInfo.eventName = node.Name;
            eInfo.cameraName = node.SelectSingleNode("CameraName").InnerText;

            XmlNode option = node.SelectSingleNode("Option");
            eInfo.unlock = option.Attributes["QuestListUnlock"].Value;

            if (option.Attributes["GetOnStateID"] != null)
            {
                string onId = option.Attributes["GetOnStateID"].Value;
                string[] IDs = onId.Split('/');
                eInfo.getOnStateID = IDs;
            }
            else eInfo.getOnStateID = null;
            eventList.Add(eInfo);
        }

    }


    public void ActiveEvent(string eventName)
    {
        if (eventName.Contains("Scene"))
        {
          //  Debug.Log(eventName);
            string scene = eventName.Replace("Scene_", "");
            GameManager.Instance.SetNextScene((SceneType)Enum.Parse(typeof(SceneType), scene));

            if(!GameManager.Instance.bPlayEvent)
                 GameManager.Instance.NextScene();
        }
        else if (eventName == "Criminal")
        {
            UIManager.Instance.StartCriminal = true;
            UIManager.Instance.PlayCriminal();
        }
        else if (eventName == "Final")
        {
            UIManager.Instance.StartCriminal = true;
            UIManager.Instance.PlayFinal();
        }
        else
        {
            UIManager.Instance.StartFadeIn();

            // 같은 이름 찾기
            string camName = null;
            string qId = null;
            foreach (EventInfo e in eventList)
            {
                if (e.cameraName == eventName)
                {
                    camName = e.cameraName;
                    qId = e.unlock;
                    break;
                }
            }

#if UNITY_EDITOR
            if (camName == null)
                Debug.Log(name + ": EventCamera Error");
#endif

            // 퀘스트 언락
            UIManager.Instance.Quest.AddObserveQuest(qId);

            UIManager.Instance.Quest.SetObserve(true);

            UIManager.Instance.StartFadeIn();

            GameManager.Instance.PlayerEnable(false);

            GameManager.Instance.bPlayEvent = true;

            // 카메라 변경
            foreach (GameObject camera in _eventCamera)
            {
                if (camera.name == camName)
                {

                    camera.SetActive(true);
                    camera.GetComponent<EventCamera>().SetID(qId);
                    _mainCamera = GameManager.Instance.Main;
                    _mainCamera.SetActive(false);
                    break;
                }
            }
        }
    }

    GameObject _action;
    public void StartActionCam(string cameraName)
    {
        if (_action != null)
        {
            _action.SetActive(false);
            _action = null;
        }
        for (int i = 0; i < actionCamera.Length; i++)
        {
            if (actionCamera[i].name == cameraName)
            {
                _action = actionCamera[i];
                _action.SetActive(true);
                break;
            }
        }

#if UNITY_EDITOR
        if (_action == null) Debug.Log("No Action");
#endif
    }
  
    public void EndAction()
    {
        _action.SetActive(false);
        _action = null;
    }

    GameObject ch;
    public void StartSpawn(string objName)
    {
        if (ch != null)
        {
            ch.SetActive(false);
            ch = null;
        }
        for (int i = 0; i < spwan.Length; i++)
        {
            if (spwan[i].name == objName)
            {
                ch = spwan[i];
                ch.SetActive(true);
                break;
            }
        }

#if UNITY_EDITOR
        if (ch == null) Debug.Log("No Action");
#endif
    }


}
