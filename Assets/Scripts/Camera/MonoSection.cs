﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoSection : MonoBehaviour
{
    [Header("해당지역 독백 시작 ID")]
    [SerializeField]
    private int MonoID;
    [SerializeField]
    GameObject cam;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.IsObserve = true;

            if (cam != null)
                cam.SetActive(true);

            UIManager.Instance.Dialoue.SetActive(true);
            DialoguePanel.Instance.Monologue(MonoID);


            this.gameObject.SetActive(false);
        }
    }
}
