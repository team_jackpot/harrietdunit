﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class EventCamera : MonoBehaviour
{

    int itemLayer = (1 << 29) +(1 << 27);
    Camera _camera;
    DetailsCamera _det;

    string qId;
    public string Qid { get { return qId; } }
    public void SetID(string id) { qId = id; }

    GameObject ui;
    Text txt_name;

    bool b_endEvnt = false;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _det = GetComponent<DetailsCamera>();

        ui = UIManager.Instance.UI_ob;
        txt_name = ui.GetComponentInChildren<Text>();
    }

    bool init = false;
    private void OnEnable()
    {
        if (!init) { init = true;}
        else
        {
            Invoke("StartEvent", 1.0f);
        }
    }

    void StartEvent()
    {
        b_endEvnt = false;
    }

    private void Update()
    {
        if (!DialoguePanel.Instance.IsTalk && !b_endEvnt && !UIManager.Instance.OpenNote)
        {
            Interaction();

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EndEvent();
            }
        }
        else ui.SetActive(false);
    }

    //클릭 시 
    void Interaction()
    {
        Ray r = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, 8.0f, itemLayer))
        {
            if (hit.collider.gameObject.layer == 29)
            {
                string name = hit.collider.name;

                ui.SetActive(true);
                txt_name.text = name;
                SetFUI(Input.mousePosition);

                string objTag = hit.collider.tag;

                UIManager.Instance.SetObUI(objTag);

                if (Input.GetMouseButtonDown(0))
                {
                    UIManager.Instance.Dialoue.SetActive(true);


                    name = name.Replace(" ", "");
                    if (hit.collider.tag == "DET")
                    {
                        ui.SetActive(false);
                        DialoguePanel.Instance.StartDialogue(DialogueType.DET, name);
                        _det.enabled = true;
                        _det.DetailsObserve(name);
                        hit.collider.tag = "EXP";
                    }
                    else
                        DialoguePanel.Instance.StartDialogue(DialogueType.EXP, name);
                }
            }
        }
        else ui.SetActive(false);
    }

    public void EndEvent()
    {

        if (!GameManager.Instance.IsNext)
        {
            b_endEvnt = true;
            UIManager.Instance.StartFadeOut();
        }
        else
        {
            GameManager.Instance.bPlayEvent = false;
            GameManager.Instance.NextScene();
        }
        UIManager.Instance.Quest.SetObserve(false);
    }

    void SetFUI(Vector2 objPos)
    {
        Vector2 screenPos = objPos + new Vector2(80, -10); // _camera.WorldToScreenPoint(objPos);

        Vector2 viewPos = _camera.ScreenToViewportPoint(screenPos);
        if (viewPos.x > 0.8f)
            viewPos.x = 0.8f;
        if (viewPos.x < 0.2f)
            viewPos.x = 0.2f;

        if (viewPos.y > 0.8f)
            viewPos.y = 0.8f;
        if (viewPos.y < 0.2f)
            viewPos.y = 0.2f;

        screenPos = _camera.ViewportToScreenPoint(viewPos);

        ui.transform.position = screenPos;
    }
}