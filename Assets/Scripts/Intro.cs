﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Intro : MonoBehaviour {

    public GameObject introCam;
    public GameObject titleCam;

    [SerializeField]
    GameObject mainCam;

    public GameObject space;

    public GameObject introObj;

    public PlayableDirector timeLine;
    [SerializeField]
    GameObject[] dialog;

    bool isStart = true;

    private void OnEnable()
    {
        mainCam = GameManager.Instance.Main;
        if (!GameManager.Instance.IsPlaying)
        {
            EndIntro();
            if (!GameManager.Instance.Title)
            {
                StartIntro();
            }
            else
            {
                EndIntro();
            }
        }
        else
        {
            PlayingGame();
        }
    }

    void PlayingGame()
    {
        UIManager.Instance.OffTitle();
        GameManager.Instance.MainVirtual.SetActive(true);
        introCam.SetActive(false);
        introObj.SetActive(false);
        space.SetActive(true);
    }

    public void StartIntro()
    { 
        GameManager.Instance.Title = true;

        introCam.SetActive(true);
        introObj.SetActive(true);
        mainCam.SetActive(false);
        // 시작 버추얼 캠 끄기
        GameManager.Instance.MainVirtual.SetActive(false);

        // 카메라 영역 끄기 
        space.SetActive(false);

        // 타이틀 UI 숨기기
        UIManager.Instance.OffTitle();

        foreach (GameObject g in dialog)
            g.SetActive(false);

        timeLine.Stop();
        timeLine.Play();
    }

    void EndIntro()
    {
        if(space.activeSelf)
            space.SetActive(false);

        CinemachineVirtualCamera v = GameManager.Instance.MainVirtual.GetComponent<CinemachineVirtualCamera>();
        v.m_Priority = 1;

        introCam.SetActive(false);
        introObj.SetActive(false);

        if(!titleCam.activeSelf)
            titleCam.SetActive(true);
        UIManager.Instance.OnTitle();

        if (mainCam != null)
            mainCam.SetActive(true);
        else
        {
            mainCam = Camera.main.gameObject;
            GameManager.Instance.Main = mainCam;
        }
       
        GameManager.Instance.MainVirtual.SetActive(true);

        if (GameManager.Instance.Player.activeSelf)
            GameManager.Instance.PlayerEnable(false);

        space.SetActive(true);


    }

    public void SkipIntro()
    {
        GameManager.Instance.Title = true;
        titleCam.SetActive(true);
        timeLine.Pause();
        EndIntro();
    }

}
