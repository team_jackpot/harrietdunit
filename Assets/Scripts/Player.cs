﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    // 카메라
    [Header("시작 시 메인카메라")]
  //  [SerializeField]
    private GameObject _camera;
    private CameraCtrl _camControl;
    public void SetCamera(GameObject cam) { _camera = cam; }

    // 설정 값
    public float moveSpeed = 2.0f;
    public float rotSpeed = 5.0f;

    Rigidbody _rigi;

    //애니메이션 
    private Animator _anim;

    public float aniSpeed = 2.0f;

    private bool move;

    bool btnDown = false;

    bool _bTalking = false;               //Check Talking

    Vector3 moveVec;
    float v;
    float h;
    void Start()
    {

        GameManager.Instance.Player = this.gameObject;
        //카메라 설정
        _camera = Camera.main.gameObject;
        
      _camControl = _camera.GetComponent<CameraCtrl>();

        _rigi = GetComponent<Rigidbody>();
        //애니메이션 셋팅
        _anim = transform.GetComponentInChildren<Animator>();


       // lerpTime = Time.deltaTime / 0.2f;
    }

    void LateUpdate()
    {
        if (!DialoguePanel.Instance.IsTalk && !UIManager.Instance.OpenNote && 
            !UIManager.Instance.StartCriminal && !UIManager.Instance.StartGame)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) ||
                Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                v = Input.GetAxisRaw("Vertical");
                h = Input.GetAxisRaw("Horizontal");

                moveVec = VecForward() * v + _camera.transform.right * h;

                transform.rotation = Quaternion.LookRotation(moveVec);
                transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);

                _anim.SetBool("btn", true);
            }
            else
            {
                _anim.SetBool("btn", false);
            }
        }
        else _anim.SetBool("btn", false);
    }


    void Move()
    {
        float v = -Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");
        moveVec.Set(v, 0, h);
        moveVec = moveVec.normalized * moveSpeed * Time.deltaTime;
        transform.Translate ( moveVec,Space.World);

    }


    // 카메라 forward 구하는 함수
    private Vector3 VecForward()
    {
        Vector3 right = _camera.transform.right;

        Vector3 up = Vector3.up;

        //외적
        Vector3 cross = Vector3.Cross(right, up).normalized;

        return cross;
    }

    private bool ViewForward()
    {
        Vector3 charVec = transform.forward;
        Vector3 camVec = VecForward();

        float dot = Vector3.Dot(charVec, camVec);

        if (dot > 0.9f)
        {
            return true;
        }

        Quaternion quat = _camera.transform.rotation;
        quat.x = 0; quat.z = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, quat, 0.1f);

        return false;
    }

    [SerializeField]
    AudioClip clip;
    public void Walk()
    {
        GameManager.Instance.Audio.PlayOneShot(clip);
    }
}
