﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MatchingGameManager : MonoBehaviour {
    private int size;
    private GameObject[,] p;
    private GameObject cube;
    void Awake () {
        size = 3;
        Init();
        cube = new GameObject();
    }
    void Update () {
        if(Input.GetMouseButtonDown(0))
            MouseClickCheck();
	}
    void MouseClickCheck(){
        //카메라에서 마우스위치로 레이를 쏨
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            int n = hit.collider.gameObject.GetComponent<Block>().Number;
            int j = n % size;
            int i = (n - j) / size;
            StateChange(hit.collider.gameObject, i, j);
            Debug.Log("Number = " + n + ", [" + i + ", " + j + "]");
        }
        if (StateCheck()) Debug.Log("Clear~!~!!~!~!!!");
    }
    bool StateCheck()
    {
        int check=0;
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                if(p[i, j].GetComponent<Block>().On)
                {
                    check++;
                }
            }
        }
        Debug.Log("Ckeck Count = " + check);
        return check == (size * size - 1) ? true : false;
    }
    void StateChange(GameObject obj,int x,int y)
    {
        for(int i = x-1; i <= x+1; i++){
            for(int j = y-1; j <= y+1; j++){
                //클릭된 부분(x,y)의 팔방면상태변환시키기
                if (i < 0 || i > size - 1 ) continue;
                if (j < 0 || j > size - 1 ) continue;
                //if (i == x && j == y) continue;
                p[i, j].GetComponent<Block>().StateChange();
            }
        }
    }
    void Init(){
        int a = 0;
        cube = Resources.Load("Cube") as GameObject;
        if (cube == null) { Debug.Log("Cube 로드실패"); return; }
        p = new GameObject[size, size];
        for (int i=0; i< size; i++){
            for (int j=0; j< size; j++){
                p[i, j] = Instantiate(cube, new Vector3(i, j, 0), cube.transform.rotation);
                p[i, j].GetComponent<Block>().setInit(a++);
            }
        }
    }
}
