﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class RecordPage : MonoBehaviour
{
    bool[] isSave = new bool[8]; //저장되어있는 슬롯인지
    GameObject[] saveSlot = new GameObject[8];

    TimeSpan time_played;

    Slider soundSlider;

    private void Awake()
    {
        soundSlider = GetComponentInChildren<Slider>();
        InitSlot();
    }

    void InitSlot()
    {
        for (int i = 0; i < 8; i++)
        {
            saveSlot[i] = transform.GetChild(0).GetChild(0).GetChild(i).gameObject;
            FileInfo fi = new FileInfo(Application.dataPath + "/SaveData_" + i + ".xml");

            if (fi.Exists)
            {
                isSave[i] = true;

                saveSlot[i].GetComponent<Button>().enabled = false;
                // 버튼 활성화
                saveSlot[i].transform.GetChild(4).gameObject.SetActive(true);
                saveSlot[i].transform.GetChild(5).gameObject.SetActive(true);

                //플레이 타임, 장소, 저장시간 가져오기
                XmlDocument saveDoc = new XmlDocument();
                saveDoc.Load(Application.dataPath + "/SaveData_" + i + ".xml");

                string str_date = saveDoc.SelectSingleNode("Save/Date").InnerText;
                saveSlot[i].transform.GetChild(1).GetComponent<Text>().text = str_date;

                string str_time = saveDoc.SelectSingleNode("Save/PlayTime").InnerText;
                saveSlot[i].transform.GetChild(2).GetComponent<Text>().text = str_time;

                string str_location = saveDoc.SelectSingleNode("Save/Location").InnerText;
                saveSlot[i].transform.GetChild(3).GetComponent<Text>().text = str_location;
            }
            else
            {
                isSave[i] = false;
                saveSlot[i].GetComponent<Button>().enabled = true;
                saveSlot[i].transform.GetChild(4).gameObject.SetActive(false);
                saveSlot[i].transform.GetChild(5).gameObject.SetActive(false);
            }
        }
    }

    public void Save(int num)
    {
        //새로 저장일 경우
        if (!isSave[num])
        {
            isSave[num] = true;

            saveSlot[num].GetComponent<Button>().enabled = false;
            saveSlot[num].transform.GetChild(4).gameObject.SetActive(true);
            saveSlot[num].transform.GetChild(5).gameObject.SetActive(true);
        }

        // 저장 시간
        string str_date = DateTime.Now.ToString("yyyy/MM/dd");
        saveSlot[num].transform.GetChild(1).GetComponent<Text>().text = str_date;

        // 플레이 타임 
        DateTime start = GameManager.Instance.StartTime;
        DateTime end = DateTime.Now;
        TimeSpan time = end - start;

        TimeSpan now = time_played.Add(time);
        string str_time = string.Format("{0:00}:{1:00}:{2:00}", now.Hours, now.Minutes, now.Seconds);
        saveSlot[num].transform.GetChild(2).GetComponent<Text>().text = str_time;

        // 장소 
        string str_location = GameManager.Instance.CurScene.ToString();
        saveSlot[num].transform.GetChild(3).GetComponent<Text>().text = str_location;

        //xml
        // 1. 현재 사용 중인 파일 가져오기
        XmlDocument objDoc = new XmlDocument();
        objDoc.Load(Application.dataPath + "/Play_ob.xml");
        XmlDocument noteDoc = new XmlDocument();
        noteDoc.Load(Application.dataPath + "/Play_note.xml");
        XmlDocument QLDoc = new XmlDocument();
        QLDoc.Load(Application.dataPath + "/Play_QL.xml");
        XmlDocument sDoc = new XmlDocument();
        sDoc.Load(Application.dataPath + "/Play_select.xml");

        // 2. 슬롯 번호에 맞게 저장
        objDoc.Save(Application.dataPath + "/Play_ob" + num + ".xml");
        noteDoc.Save(Application.dataPath + "/Play_note" + num + ".xml");
        QLDoc.Save(Application.dataPath + "/Play_QL" + num + ".xml");
        sDoc.Save(Application.dataPath + "/Play_select" + num + ".xml");

        // save xml 만들기
        XmlDocument saveDoc = new XmlDocument();
        XmlElement root = saveDoc.CreateElement("Save");

        saveDoc.AppendChild(root);

        XmlElement date = saveDoc.CreateElement("Date");
        date.InnerText = str_date;
        root.AppendChild(date);

        XmlElement playTime = saveDoc.CreateElement("PlayTime");
        playTime.InnerText = str_time;
        root.AppendChild(playTime);

        XmlElement location = saveDoc.CreateElement("Location");
        location.InnerText = str_location;
        root.AppendChild(location);

        saveDoc.Save(Application.dataPath + "/SaveData_" + num + ".xml");
    }

    public void Load(int num)
    {
        //플레이 타임, 장소 가져오기
        XmlDocument saveDoc = new XmlDocument();
        saveDoc.Load(Application.dataPath + "/SaveData_" + num + ".xml");

        string str_time = saveDoc.SelectSingleNode("Save/PlayTime").InnerText;
        time_played = TimeSpan.Parse(str_time);

        string str_location = saveDoc.SelectSingleNode("Save/Location").InnerText;
        SceneType type = (SceneType)Enum.Parse(typeof(SceneType), str_location);
        GameManager.Instance.SetNextScene(type);

        string str_check = saveDoc.SelectSingleNode("Save/DrRoom").InnerText;
        bool b = bool.Parse(str_check);

        //xml 
        // 단서
        XmlDocument doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_ob" + num + ".xml");
        doc.Save(Application.dataPath + "/Play_ob.xml");
        // 수첩
        doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_note" + num + ".xml");
        doc.Save(Application.dataPath + "/Play_note.xml");
        // 퀘스트 리스트
        doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_QL" + num + ".xml");
        doc.Save(Application.dataPath + "/Play_QL.xml");
        // 선택지
        doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_select" + num + ".xml");
        doc.Save(Application.dataPath + "/Play_select.xml");

       // GameManager.Instance.IsLoad = true;
   //     UIManager.Instance.Quest.CheckedQuest();

        //시작 시간 재 설정
        GameManager.Instance.SetStratTime();
        // 이동
        GameManager.Instance.NextScene();
    }

    public void SoundSize()
    {
        AudioListener.volume = soundSlider.value;
    }

    public void MoveToTitle()
    {
        this.gameObject.SetActive(false);
        GameManager.Instance.SetNextScene(SceneType.Title);
        GameManager.Instance.NextScene();
    }
}
