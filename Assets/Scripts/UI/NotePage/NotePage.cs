﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public struct Slot
{
    public int id;   //테이블 아이디
    public string name;      // 이름
    public string prefabName; // 설명 프리팹 이름 
    public string evidenceDesc;

    public Slot(int _id, string _name, string _prefabName, string _evidenceDesc)
    {
        id = _id;
        name = _name;
        prefabName = _prefabName;
        evidenceDesc = _evidenceDesc;
    }
}

enum CH
{
    Doctor = 0,
    Rucien,
    Modo,
    Suji,
    Victoria,
    Willie
};

public class NotePage : MonoBehaviour
{
    //인물
    RectTransform _CharRect;

    GameObject _charRight;
    Image charImg;
    Text char_name;
    Text char_info;
    Text char_script;

    List<Slot> charList;
    List<GameObject> ui_charList;

    //증거
    RectTransform _clueRect;

    GameObject _clueRight;
    Text clue_name;
    Text clue_script;

    List<Slot> clueList;
    List<GameObject> ui_clueList;
    //
    List<int> questChkList; //퀘스트 체크용 단서 

    XmlDocument doc;

    bool _isInit = false;
    private void Awake()
    {
        //인물용
        _CharRect = transform.GetChild(0).GetChild(2).GetComponent<RectTransform>();
        _charRight = transform.GetChild(1).GetChild(0).gameObject;
        charImg = _charRight.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        char_name = _charRight.transform.GetChild(1).GetComponent<Text>();
        char_info = _charRight.transform.GetChild(2).GetComponent<Text>();
        char_script = _charRight.transform.GetChild(3).GetComponent<Text>();
        //증거용
        _clueRect = transform.GetChild(0).GetChild(3).GetComponent<RectTransform>();
        _clueRight = transform.GetChild(1).GetChild(1).gameObject;
        clue_name = _clueRight.transform.GetChild(0).GetComponent<Text>();
        clue_script = _clueRight.transform.GetChild(1).GetComponent<Text>();
    }

    private void OnEnable()
    {
        if (_isInit)
        {
            SetCharPage();
            SetCluePage();
        }
    }

    private void OnDisable ()
    {
        if(!_isInit) InitXml();
    }

    public void NewGame()
    {
        if (charList != null)
        {
            charList.Clear();
            clueList.Clear();
            questChkList.Clear();

            foreach (GameObject g in ui_charList)
            {
                g.transform.SetParent(null);
                Destroy(g);
            }
            ui_charList.Clear();

            foreach (GameObject ui in ui_clueList)
            {
                ui.transform.SetParent(null);
                Destroy(ui);
            }
            ui_clueList.Clear();
        }
    }

    public void Init()
    {
        // 리스트 초기화
        charList = new List<Slot>();
        ui_charList = new List<GameObject>();
        clueList = new List<Slot>();
        ui_clueList = new List<GameObject>();
        questChkList = new List<int>();
    }

    void InitXml() {
        _isInit = true;
        // xml 초기화
        doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_note.xml");

        // on  상태인 인물 단서 찾기 
        XmlNodeList charNodeList = doc.SelectSingleNode("Note/Char/Clue[@DefaultState='on']").ChildNodes;
        foreach (XmlNode node in charNodeList)
        {
            string str_id = node.Attributes["id"].Value;
            int _id = int.Parse(str_id);

            if (_id >= 39000)
            {
                questChkList.Add(_id);
            }
            else
            {
                string _KrName = node.SelectSingleNode("EvidenceNameKr").InnerText;

                string _prefabName = null;
                if (node.SelectSingleNode("EvidenceDefaultName") != null)
                    _prefabName = node.SelectSingleNode("EvidenceDefaultName").InnerText;

                string _desc = node.SelectSingleNode("EvidenceDesc").InnerText;

                if (node.SelectSingleNode("Option") != null)
                {
                    XmlNode option = node.SelectSingleNode("Option");

                //    Note.CheckOption(option);
                }

                Slot newSlot;
                newSlot.id = _id;
                newSlot.name = _KrName;
                newSlot.prefabName = _prefabName;
                newSlot.evidenceDesc = _desc;

                charList.Add(newSlot);
            }

        }

        // on 상태인 증거 단서 찾기 
        XmlNodeList clueNodeList = doc.SelectSingleNode("Note/Clue/Clue[@DefaultState='on']").ChildNodes;
        foreach (XmlNode node in clueNodeList)
        {
            string str_id = node.Attributes["id"].Value;
            int _id = int.Parse(str_id);

            if (_id >= 39000)
            {
                questChkList.Add(_id);
            }
            else
            {
                string _KrName = node.SelectSingleNode("EvidenceNameKr").InnerText;

                string _prefabName = null;
                if (node.SelectSingleNode("EvidenceDefaultName") != null)
                    _prefabName = node.SelectSingleNode("EvidenceDefaultName").InnerText;

                string _desc = node.SelectSingleNode("EvidenceDesc").InnerText;

                if (node.SelectSingleNode("Option") != null)
                {
                    XmlNode option = node.SelectSingleNode("Option");

                 //   Note.CheckOption(option);
                }

                Slot newSlot;
                newSlot.id = _id;
                newSlot.name = _KrName;
                newSlot.prefabName = _prefabName;
                newSlot.evidenceDesc = _desc;

                clueList.Add(newSlot);
            }
        }
        SetCharPage();
        SetCluePage();
    }

    public void AddChar(Slot slot)
    {
        if(!charList.Contains(slot))
            charList.Add(slot);
    }
    public void AddClue(Slot slot)
    {
        if (!clueList.Contains(slot))
            clueList.Add(slot);
    }
    public void Add(int id)
    {
        if (!questChkList.Contains(id))
            questChkList.Add(id);
    }

    // 인물 페이지 ui 
     void SetCharPage()
    {
        int count = charList.Count;
        int charVal = count - ui_charList.Count;
        for (int i = ui_charList.Count; i < count; i++)
        {
            GameObject slot = null;
            GameObject prefab = Resources.Load("Prefabs/Note/" + charList[i].prefabName) as GameObject;
                slot = Instantiate(prefab);

            slot.name = charList[i].prefabName;

            slot.transform.SetParent(_CharRect);

            int uiCount = ui_charList.Count;
            if (uiCount >= 4)
            {
                _CharRect.sizeDelta = new Vector2(600, uiCount * 200);
            }
            RectTransform rect = slot.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, uiCount * -200);

            int num = i;
            Button btn = slot.GetComponent<Button>();
            btn.onClick.AddListener(() => CharClick(num));

            ui_charList.Add(slot);
        }
    }

  
    //인물 단서 클릭 
    void CharClick(int num)
    {
        _charRight.SetActive(true);

        CH ch = (CH)Enum.Parse(typeof(CH), charList[num].prefabName);
        int i = (int)Convert.ChangeType(ch, ch.GetTypeCode());

        Sprite[] sprites = null;
        if (i <3)   
            sprites = Resources.LoadAll<Sprite>("Note/Img/Propile_0");
        else
             sprites = Resources.LoadAll<Sprite>("Note/Img/Propile_1");

        Sprite sp = new Sprite();
        foreach(Sprite sprite in sprites)
        {
            if (sprite.name == charList[num].prefabName + "_long")
            {
                sp = sprite;
                break;
            }
        }
        // 이미지 변경
        charImg.sprite = sp;//Resources.Load<Sprite>("Note/Img/" + charList[num].prefabName+"_long");

        // 내용 변경
        string[] des = charList[num].evidenceDesc.Split('/');
        char_name.text = des[0];
        char_info.text = des[1];
        char_script.text = des[2];
    }


    // 증거 페이지 ui 
     void SetCluePage()
    {
        int count = clueList.Count;
        for (int i = ui_clueList.Count; i < count; i++)
        {
            GameObject slot = null;
            GameObject pre= Resources.Load("Prefabs/Note/cluePrefab") as GameObject;

            slot = Instantiate(pre);
            slot.name = clueList[i].name;

            slot.GetComponentInChildren<Text>().text = clueList[i].name;
            slot.transform.SetParent(_clueRect);

            int num = i;
            int uiCount = num;
            if (uiCount >= 11)
            {
                _clueRect.sizeDelta = new Vector2(600, uiCount * 60);
            }
            RectTransform rect = slot.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, num * -60);

            
            Button btn = slot.GetComponent<Button>();
            btn.onClick.AddListener(() => ClueClick(num));

            ui_clueList.Add(slot);
        }

    }

    void ClueClick(int num)
    {
        _clueRight.SetActive(true);

        clue_name.text = clueList[num].name;
        clue_script.text = clueList[num].evidenceDesc;
    }

}
