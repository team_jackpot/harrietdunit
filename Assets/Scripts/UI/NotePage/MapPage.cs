﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPage : MonoBehaviour {
    [SerializeField]
    GameObject doc;
    [SerializeField]
    GameObject modo;
    [SerializeField]
    GameObject btn;
    UIButton script;

    RectTransform[] local;
    GameObject player;
    RectTransform rect;

    GameObject impossableImg;

    bool isChange = false;

    string cur = "Street";
    string next = "";
    private void Awake()
    {
        script = btn.GetComponent<UIButton>();
        player = transform.GetChild(6).gameObject;
        rect = player.GetComponent<RectTransform>();
        local = new RectTransform[5];
        for(int i=0; i<5; i++)
        {
            local[i] = transform.GetChild(7).GetChild(i).GetComponent<RectTransform>();
        }
        impossableImg = transform.GetChild(9).gameObject;
    }

    private void OnEnable()
    {
        string now = GameManager.Instance.CurScene.ToString();
        string chk = null;
        if (now.Contains("Night"))
        {
            if (now.Contains("Library")) chk = "Tu_Library";
            else if (!now.Contains("Modo")) chk = now.Replace("_Night", "");
            else chk = now;
        }
        else chk = now;

        if (chk != cur)
        {
            int num = -1;
            if (chk.Contains("Tu")) num = 0;
            else if (chk.Contains("P")) num = 1;
            else if (chk.Contains("St")) num = 2;
            else if (chk.Contains("Doc")) num = 3;
            else if (chk.Contains("Modo")) num = 4;

            cur = chk;
            rect.anchoredPosition = local[num].anchoredPosition;
        }

        if (!DialoguePanel.Instance.IsTalk && !GameManager.Instance.IsObserve
            && !UIManager.Instance.StartGame && !UIManager.Instance.StartCriminal)
            impossableImg.SetActive(false);
        else
        {
            if (!impossableImg.activeSelf)
                impossableImg.SetActive(true);
        }
    }

    public void NewGame()
    {
        isChange = false;
        btn.SetActive(false);

        doc.SetActive(false);

        modo.SetActive(false);
    }

    public void Click(string scene)
    {

        string str_scene = scene;
        if (isChange && !str_scene.Contains("Night"))
        {
            if (str_scene.Contains("Tu"))
                str_scene = "Library_Night";
            else
                str_scene = scene + "_Night";
        }

        if (cur != scene)
        {
            next = scene;
            GameManager.Instance.SetNextScene((SceneType)Enum.Parse(typeof(SceneType), str_scene));
            StartCoroutine(MovePlayer());
        }
    }
    IEnumerator MovePlayer()
    {
        int num = -1;
        if (next.Contains("Tu")) num = 0;
        else if (next.Contains("P")) num = 1;
        else if (next.Contains("St")) num = 2;
        else if (next.Contains("Doc")) num = 3;
        else if (next.Contains("Modo")) num = 4;
        else Debug.Log("Map Name Error");

        cur = next;
        Vector2 pos = local[num].anchoredPosition;

        while (Vector2.Distance(rect.anchoredPosition,pos) > 0.01f)
        {
            rect.anchoredPosition = Vector3.MoveTowards(rect.anchoredPosition, pos, 10);
            yield return new WaitForSeconds(Time.deltaTime);
        }

        yield return new WaitForSeconds(0.1f);

        script.SetClick();
        GameManager.Instance.NextScene();
        UIManager.Instance.Note.gameObject.SetActive(false);
        UIManager.Instance.OpenNote = false;
        yield break;

    }

    public void OpenMap(string map)
    {
        string re = map.Replace("Map_", "");
        if (re == "Map")
            btn.SetActive(true);
        if (re == "Doc")
            doc.SetActive(true);
        else if (re.Contains("Modo"))
            modo.SetActive(true);
    }

    public void ChangeNight()
    {
        isChange = true;
    }
}
