﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    // 싱글 톤
    private static UIManager _instance = null;
    public static UIManager Instance { get { return _instance; } }

    GameObject ui_tile;
    GameObject ui_game;
    GameObject ui_criminal;

    GameObject ui_panel;
    Image panel;

    [SerializeField]
    AudioClip[] bgm;

    bool _openNote = false;
    public bool OpenNote
    { get { return _openNote; }
        set { _openNote = value; }
    }
    //----------------- 수첩  ---------------------
    private GameObject panel_Note;
    private NotePanel _note;
    public NotePanel Note { get { return _note; } }

    // ----------------- 대화 ---------------------
    private GameObject panel_Dialogue;
    public GameObject Dialoue { get { return panel_Dialogue; } }

    //------------------- 퀘스트 ------------------------
    private QuestSystem _quest;
    public QuestSystem Quest { get { return _quest; } }

    // 선택지
    private SelectPanel _select;
    public SelectPanel Select { get { return _select; } }

    //-----------------미니게임--------------------------
    private RebutDialogPanel _reburt;
    private GameObject ui_reburt;
    private GameObject ui_crim;

    private GameObject ui_f;
    public GameObject UI_ob { get { return ui_f; } }
    Image ob_img;
    [SerializeField]
    Sprite[] sp;

    bool _startGame = false;
    public bool StartGame { get { return _startGame; } set { _startGame = value; } }
    bool _startCrim = false;
    public bool StartCriminal { get { return _startCrim; } set { _startCrim = value; } }

    private void Awake()
    {
        // 씬에 이미 게임 매니저가 있을 경우
        if (_instance)
        {
            //삭제
            Destroy(gameObject);
            return;
        }

        // 유일한 게임 매니저 
        _instance = this;
        DontDestroyOnLoad(gameObject);

        // -------------- 게임 오브젝트 초기화 ----------------

        ui_f = transform.GetChild(0).GetChild(0).gameObject;
        ob_img = ui_f.GetComponent<Image>();

        ui_criminal = transform.GetChild(1).gameObject;
        ui_criminal.SetActive(false);
        ui_crim = ui_criminal.transform.GetChild(0).gameObject;
        ui_reburt = ui_criminal.transform.GetChild(1).gameObject;
        _reburt = ui_reburt.GetComponent<RebutDialogPanel>();

        ui_game = transform.GetChild(2).gameObject;
        ui_game.SetActive(false);
        ui_tile = transform.GetChild(3).gameObject;
        ui_tile.SetActive(false);

    }

    public void Init()
    {
        ui_tile.SetActive(false);

        ui_game.SetActive(true);
        ui_criminal.SetActive(true);
        _quest = transform.GetComponentInChildren<QuestSystem>();

        // 인게임
        panel_Note = ui_game.transform.GetChild(4).gameObject;
        _note = panel_Note.GetComponent<NotePanel>();

        _note.NewGame();
        _note.Init();
        panel_Note.SetActive(false);

        panel_Dialogue = ui_game.transform.GetChild(2).gameObject;
        panel_Dialogue.SetActive(false);

        ui_panel = ui_game.transform.GetChild(5).gameObject;
        panel = ui_panel.GetComponent<Image>();
        ui_panel.SetActive(false);

        _quest.NewGame();
         _quest.InitQuest();

        _select = GetComponentInChildren<SelectPanel>();

    }

    public void SetObUI(string tag)
    {
        switch (tag)
        {
            case "DET":
                ob_img.sprite = sp[1];
                break;
            case "EXP":
                ob_img.sprite = sp[2];
                break;
            default:
                ob_img.sprite = sp[0];
                break;
        }
    }

    public void PlayCriminal()
    {
        GameManager.Instance.Audio.clip = bgm[1];
        if (!GameManager.Instance.Audio.isPlaying)
            GameManager.Instance.Audio.Play();

        ui_crim.SetActive(true);
    }
    public void PlayMiniGame()
    {
        GameManager.Instance.Audio.clip = bgm[0];
        if (!GameManager.Instance.Audio.isPlaying)
            GameManager.Instance.Audio.Play();

        GameManager.Instance.IsObserve = true;
        ui_reburt.SetActive(true);
        _reburt.StartRebut(CriminalType.Mini);
    }
    public void PlayFinal()
    {
        _startCrim = true;
        ui_reburt.SetActive(true);
        _reburt.StartRebut(CriminalType.Final);
    }

    public void MoveToTitle()
    {
        GameManager.Instance.Title = true;
        GameManager.Instance.SetNextScene(SceneType.Title);
        GameManager.Instance.NextScene();
    }

    public void OnTitle()
    {
        ui_tile.SetActive(true);
    }
    public void OffTitle()
    {
        ui_tile.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            if (panel_Note.activeSelf)
                panel_Note.SetActive(false);
            else
            {
                panel_Note.SetActive(true);
                Note.Click(0);
            }
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (panel_Note.activeSelf)
                panel_Note.SetActive(false);
        }
    }

    //============================================
    //         씬 변경시 유아이 설정
    //============================================
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }


    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
       SceneType cur = GameManager.Instance.CurScene;

        if (cur == SceneType.Title)
        {
            ui_tile.SetActive(true);
            ui_game.SetActive(false);
        }
        else
        {
        
            ui_tile.SetActive(false);

            ui_game.SetActive(true);

            //if (GameManager.Instance.IsLoad)
            //{
                _quest.InitQuest();
            //    _note.Init();
            //}
            //else
            //    GameManager.Instance.IsLoad = false;

            
        }
    }

    //============================================
    //        Fade In / Out
    //============================================

    public void StartFadeIn()
    {
        ui_panel.SetActive(true);
        StartCoroutine(FadeIn());
    }
    public void StartFadeOut()
    {
        ui_panel.SetActive(true);
          StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn()
    {
        Color color = Color.black;
        while (color.a > 0)
        {
            color.a -= Time.deltaTime;
            panel.color = color;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        ui_panel.SetActive(false);
        yield break;
    }

    IEnumerator FadeOut()
    {
        Color color = panel.color;
        while (color.a < 1)
        {
            color.a += Time.deltaTime;
            panel.color = color;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        EventCamera e = GameObject.FindObjectOfType<EventCamera>();
        e.transform.gameObject.SetActive(false);

        GameObject _main = GameManager.Instance.Main;
        _main.SetActive(true);
        GameManager.Instance.PlayerEnable(true);
        while (color.a > 0)
        {
            color.a -= Time.deltaTime;
            panel.color = color;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        ui_panel.SetActive(false);

        yield return new WaitForSeconds(3.0f);

        GameManager.Instance.bPlayEvent = false;
        if (GameManager.Instance.IsNext) GameManager.Instance.NextScene();

        if (_startGame)
            PlayMiniGame();

        yield break;
    }

}
