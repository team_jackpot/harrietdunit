﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class NotePanel : UIPanel
{
    NotePage note;
    InfPage inf;

    ScrollRect left;
    Scrollbar bar;

    GameObject char_scroll;
    GameObject clue_scroll;
    RectTransform char_L;
    RectTransform clue_L;
    GameObject char_R;
    GameObject clue_R;

    GameObject map;
    MapPage mapP;

    GameObject record;

    int open = -1;

	void Awake ()
    {
        note = transform.GetChild(1).GetComponent<NotePage>();
        bar = transform.GetChild(1).GetChild(0).GetComponentInChildren<Scrollbar>();

        left = transform.GetChild(1).GetChild(0).GetComponent<ScrollRect>();
        char_scroll = transform.GetChild(1).GetChild(0).GetChild(0).gameObject;
        clue_scroll = transform.GetChild(1).GetChild(0).GetChild(1).gameObject;
        char_L = transform.GetChild(1).GetChild(0).GetChild(2).GetComponent<RectTransform>();
        clue_L = transform.GetChild(1).GetChild(0).GetChild(3).GetComponent<RectTransform>();

        char_R = transform.GetChild(1).GetChild(1).GetChild(0).gameObject;
        clue_R = transform.GetChild(1).GetChild(1).GetChild(1).gameObject;

        //  inf = transform.GetChild(1).GetChild(4).GetComponent<InfPage>();
        record = transform.GetChild(2).gameObject;

        map = transform.GetChild(3).gameObject;
        mapP = map.GetComponent<MapPage>();
    }

    public void Init()
    {
        note.Init();
        // 추론 
    }

    private void OnEnable()
    {
        UIManager.Instance.OpenNote = true;
    }
    private void OnDisable()
    {
        UIManager.Instance.OpenNote = false;
    }

    public void Click(int num)
    {
        if (open != num)
        {
          

            char_R.SetActive(false);
            clue_R.SetActive(false);

            open = num;
            switch (num)
            {
                case -1:
                   // left.content = char_L;
                   // char_L.gameObject.SetActive(true);
                   // clue_L.gameObject.SetActive(false);
                   // clue_R.gameObject.SetActive(false);
                  //  char_scroll.SetActive(true);
                   // char_scroll.SetActive(false);
                  //  map.SetActive(false);
                  //  record.SetActive(false);

                    break;
                case 0:
                    if (!note.gameObject.activeSelf)
                        note.gameObject.SetActive(true);
                    left.content = char_L;
                    char_L.gameObject.SetActive(true);
                    clue_L.gameObject.SetActive(false);
                    clue_R.gameObject.SetActive(false);
                    clue_scroll.SetActive(false);
                    char_scroll.SetActive(true);
                    map.SetActive(false);
                    record.SetActive(false);
                    break;

                case 1:
                    if (!note.gameObject.activeSelf)
                        note.gameObject.SetActive(true);
                    left.content = clue_L;
                    char_L.gameObject.SetActive(false);
                    clue_L.gameObject.SetActive(true);
                    char_R.gameObject.SetActive(false);
                    clue_scroll.SetActive(true);
                    char_scroll.SetActive(false);
                    map.SetActive(false);
                    record.SetActive(false);
                    break;

                case 2:
                    map.SetActive(true);
                    note.gameObject.SetActive(false);
                    record.SetActive(false);
                    break;

                case 3:
                    note.gameObject.SetActive(false);
                    record.SetActive(true);
                    map.SetActive(false);
                    break;

                default:
                    break;
            }
        }
    }

    public void NewGame()
    {
        note.NewGame();
        mapP.NewGame();
    }
    public void MapOpen(string str)
    {
        mapP.OpenMap(str);
    }
    public void MapChange()
    {
        mapP.ChangeNight();
    }

    //초기화

    // 단서 추가 

    public void Add(int checkID)
    {
        XmlDocument doc = new XmlDocument();


        doc.Load(Application.dataPath + "/Play_note.xml");
        XmlNodeList typeList = doc.SelectSingleNode("Note").ChildNodes;
        foreach (XmlNode type in typeList)
        {
            XmlNode node = type.SelectSingleNode("Clue[@ID='" + checkID + "']");
            if (node != null)
            {
                node.Attributes["DefaultState"].Value = "on";
                doc.Save(Application.dataPath + "/Play_note.xml");

                string name = node.SelectSingleNode("EvidenceNameKr").InnerText;

                if (name == "n")
                {
                    note.Add(checkID);
                }
                else
                {
                    string _prefabName = null;
                    if (node.SelectSingleNode("EvidenceDefaultName") != null)
                    {
                        _prefabName = node.SelectSingleNode("EvidenceDefaultName").InnerText;
                    }
                    string desc = null;
                    if (node.SelectSingleNode("EvidenceDesc") != null)
                        desc = node.SelectSingleNode("EvidenceDesc").InnerText;

                    Slot _slot = new Slot(checkID, name, _prefabName, desc);
                    if (type.Name == "Char")
                        note.AddChar(_slot);
                    else
                        note.AddClue(_slot);
                }
                XmlNode option = node.SelectSingleNode("Option");
                if (option != null)
                {
                    CheckOption(option);
                }

                Debug.Log(name + ": Add 증거 증언 :" + checkID);
                break;
            }
        }

        UIManager.Instance.Quest.QuestCheck(checkID.ToString());
    }

	public void CheckOption(XmlNode option)
    {

        if (option.Attributes["GetOffStateID"] != null)
        {
            string onStateID = option.Attributes["GetOffStateID"].Value;

            string[] IDs = onStateID.Split('/');

            for (int i = 0; i < IDs.Length; i++)
            { 
                int _id = int.Parse(IDs[i]);

                //탐색
                if (_id >= 1000 && _id < 2000)
                {
                    DialoguePanel.Instance.SetObserveState(_id, false);
                }
                //선택지
                else if (_id >= 7000 && _id < 8000)
                {

                }
                //단서
                else
                {

                }
            }
        }

        if (option.Attributes["GetOnStateID"] != null)
        {
            string onStateID = option.Attributes["GetOnStateID"].Value;

            string[] IDs = onStateID.Split('/');

            for (int i = 0; i < IDs.Length; i++)
            {
                int _id;
                bool val = int.TryParse(IDs[i], out _id);

                if (val)
                {
                    //탐색
                    if (_id >= 1000 && _id < 2000)
                    {
                        DialoguePanel.Instance.SetObserveState(_id, true);
                    }
                    //선택지
                    else if (_id >= 7000 && _id < 8000)
                    {

                    }
                    //단서 
                    else
                    {
                        Add(_id);
                    }
                }
            }
        }
        
    }
}