﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
public class RebutDialogPanel : UIPanel {

    GameObject _life;
    Image _life_bg;
    Image _lifeBar;
    [SerializeField]
    Sprite[] bg;
    RectTransform mark;
    RectTransform[] markLocal;
    int lifeCount = 5;

    Image panel;

    CriminalType _type;
    // ------------- xml -------------------
    XmlDocument doc;
    XmlNode curNode;
    TextAsset Ctxt;

    XmlDocument gDoc;
    TextAsset gTxt;
    //--------------- 대화 --------------------
    string content;          // 대화 내용 

    private bool isTurn = false;    //다음 대화
    string nextNum = null;               //다음 대화 id
    private int curPos = 0;         // 글자 순서 

    //텍스트
    private Text txt_name;          // 캐릭터 이름 
    private Text txt_chat;          // 대화 내용 

    // 이미지
    private RectTransform bg_rect;
    private Image bg_img;

    GameObject[] sel;
    UIButton[] sel_btn;

    AudioSource _audio;
    [SerializeField]
    AudioClip _clip;

    bool _action = false;


    void Awake ()
    {
        _audio = GameManager.Instance.Audio;
        Ctxt = (TextAsset)Resources.Load("Xml/Criminal");
        gTxt = (TextAsset)Resources.Load("Xml/CrimGroup");

        bg_rect = transform.GetChild(0).GetComponent<RectTransform>();
        bg_img = transform.GetChild(0).GetComponent<Image>();
        txt_name = transform.GetChild(2).GetComponent<Text>();
        txt_chat = transform.GetChild(3).GetComponent<Text>();

        _life = transform.GetChild(4).gameObject;
        _life_bg = _life.transform.GetChild(0).GetComponent<Image>();
        _lifeBar = _life.GetComponent<Image>();
        mark = _life.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>();

        markLocal = new RectTransform[5];
        for(int i=0; i<5; i++)
        {
            markLocal[i] = _life.transform.GetChild(0).GetChild(1).GetChild(i).GetComponent<RectTransform>();
        }
        
        panel = transform.GetChild(7).GetComponent<Image>();

        sel = new GameObject[3];
        sel_btn = new UIButton[3];
        for(int i = 0; i < 3; i++)
        {
            sel[i] = transform.GetChild(5).GetChild(i).gameObject;
            sel_btn[i] = sel[i].GetComponentInChildren<UIButton>();
            sel[i].SetActive(false);
        } 
    }
    private void OnEnable()
    {
        DialoguePanel.Instance.IsTalk = true;

        if (UIManager.Instance.StartGame)
        {
            lifeCount = 5;
            _life_bg.sprite = bg[0];
            _life.SetActive(false);
        }
        if (UIManager.Instance.StartCriminal)
        {
            if (!_life.activeSelf) _life.SetActive(true);
            _lifeBar.fillAmount = 0.2f * lifeCount;
        }
    }
    private void OnDisable()
    {
        DialoguePanel.Instance.IsTalk = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ClickDialogue();
    }
    public void StartRebut(CriminalType type)
    {
       
        _type = type;
        string id = "";
        switch (_type)
        {
            case CriminalType.Willie:
                id = "Sel_20001";
                break;
            case CriminalType.Modo:
                id = "Sel_30001";
                break;
            case CriminalType.Victoria:
                id = "Sel_40001";
                break;
            case CriminalType.Doctor:
                id = "Sel_50001";
                break;
            case CriminalType.Final:
                id = "Sel_60001";
                break;
            default:
                id = "Sel_10001";
                break;
        }
        doc = new XmlDocument();
        doc.LoadXml(Ctxt.text);
        XmlNode criminal = doc.SelectSingleNode("Criminal/" + _type.ToString());
        curNode = criminal.SelectSingleNode("CrimSel[@ID='" + id + "']");

        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        string str_next = curNode.SelectSingleNode("ScriptNextID").InnerText;

        // 다음 스크립트가 있을때
        if (str_next != null)
        {
            nextNum = str_next;
        }
        else nextNum = null;

        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    public void StartRebut(string str_id)
    {
        isTurn = false;

        doc = new XmlDocument();
        doc.LoadXml(Ctxt.text);

        XmlNode criminal = doc.SelectSingleNode("Criminal/" + _type.ToString());
        curNode = criminal.SelectSingleNode("CrimSel[@ID='" + str_id + "']");

        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        string str_next = curNode.SelectSingleNode("ScriptNextID").InnerText;

        // 다음 스크립트가 있을때
        if (str_next != null)
        {
            nextNum = str_next;
        }
        else nextNum = null;

        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    List<string> nextList = new List<string>();
    void SetSelect(string groupID)
    {
        gDoc = new XmlDocument();
        gDoc.LoadXml(gTxt.text);
        nextList.Clear();

        _audio.PlayOneShot(_clip);

        XmlNodeList list = gDoc.SelectNodes("CrimGroup/Think[@GroupID='" + groupID + "']");
        for (int i = 0; i < list.Count; i++)
        {
            string next = list[i].SelectSingleNode("ScriptNextID").InnerText;
            nextList.Add(next);

            //버튼
            sel[i].GetComponentInChildren<Text>().text = list[i].SelectSingleNode("ScriptDesc").InnerText;
            sel[i].SetActive(true);
        }

    }
    public void ClickSelect(int num=-1)
    {
        //선택지 끄기 
        for (int i = 0; i < 3; i++)
        {
            sel[i].SetActive(false);
           
        }

        if (num >= 0)
        {
            curPos = 0;
            sel_btn[num].SetClick();
            StartRebut(nextList[num]);
        }
    }

    void CheckOption()
    {
        if (curNode.SelectSingleNode("DownLife") != null)
        {
            if (curNode.SelectSingleNode("DownLife").InnerText == "1")
            {
                StopCoroutine(IsWrong());
                StopCoroutine(IsCorrect());
                StartCoroutine(IsWrong());

                --lifeCount;
                _lifeBar.fillAmount = 0.2f * lifeCount;

                mark.anchoredPosition = markLocal[lifeCount - 1].anchoredPosition;
                mark.rotation = markLocal[lifeCount - 1].rotation;

                if (lifeCount <= 0)
                {
                    this.gameObject.SetActive(false);
                    UIManager.Instance.Dialoue.SetActive(true);
                    if (_type == CriminalType.Final)
                    {
                        DialoguePanel.Instance.Monologue(19500);
                    }
                    else
                    {
                        DialoguePanel.Instance.Monologue(19200);
                    }
                }
                else
                {
                    if (lifeCount == 1)
                        _life_bg.sprite = bg[1];

                }
            }
            else//맞음 표시 
            {
            //    StopCoroutine(IsWrong());
            //    StopCoroutine(IsCorrect());
            //    StartCoroutine(IsCorrect());
            }
        }
        XmlNode option = curNode.SelectSingleNode("Option");
        if (option != null)
        {
            //일러스트
            if (option.Attributes["BGNameTrans"] != null)
            {
                string bgName = option.Attributes["BGNameTrans"].Value;

                // n일경우 종료
                if (bgName == "n")
                    bg_rect.gameObject.SetActive(false);
                //아닌 경우 새로 띄우거나 그림 변경
                else
                {
                    if (!bg_rect.gameObject.activeSelf) bg_rect.gameObject.SetActive(true);

                    bg_img.sprite = Resources.Load<Sprite>("Dial/" + bgName);
                    // 이미지 설정
                    bg_img.SetNativeSize();
                }
            }
            //선택지 
            if (curNode.SelectSingleNode("GroupID") != null)
            {
                SetSelect(curNode.SelectSingleNode("GroupID").InnerText);
            }
            //애니메이션 제어
            //카메라 제어 
            if (option.Attributes["CamCtrl"] != null)
            {
                string cam = option.Attributes["CamCtrl"].Value;
                if (cam == "n")
                    GameManager.Instance.Event.EndAction();
                else
                {
                    _action = true;
                    GameManager.Instance.Event.StartActionCam(cam);
                }
            }

            //캐릭터 스폰
            if (option.Attributes["ChSpwan"] != null)
            {
                string spwan = option.Attributes["ChSpwan"].Value;
                GameManager.Instance.Event.StartSpawn(spwan);
            }

        }
    }

    IEnumerator IsWrong()
    {
        Color _color = new Color(100, 11, 0, 0);
        panel.color = _color;
        while (_color.a <= 0.5f)
        {
            _color.a += 0.1f;
            panel.color = _color;
            yield return new WaitForSeconds(0.1f);
        }

        while (_color.a > 0)
        {
            _color.a -= 0.15f;
            panel.color = _color;
            yield return new WaitForSeconds(0.1f);
        }
        yield break;
    }

    IEnumerator IsCorrect()
    {
        Color _color =new  Color(0, 111, 43, 0);
        panel.color = _color;
        while (_color.a <= 0.5f)
        {
            _color.a += 0.1f;
            panel.color = _color;
            yield return new WaitForSeconds(0.1f);
        }

        while (_color.a > 0)
        {
            _color.a -= 0.15f;
            panel.color = _color;
            yield return new WaitForSeconds(0.1f);
        }
        yield break;
    }

    void NextScript(string nextID)
    {
        curNode = doc.SelectSingleNode("Criminal/" + _type + "/CrimSel[@ID='" + nextID + "']");

        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        isTurn = false;

        string str_nextId = curNode.SelectSingleNode("ScriptNextID").InnerText;

        //next id 를 판단해 다음 대사 출력
        if (str_nextId != "n")
            nextNum = str_nextId;
        else
            nextNum = null;

        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    public void ClickDialogue()
    {
        if (isTurn)
        {
            ClickSelect();
            if (nextNum != null)
            {
                NextScript(nextNum);
            }
            else
            {
                XmlNode option = curNode.SelectSingleNode("Option");
               

                if (option.Attributes["MonoScriptID"] != null)
                {
                    int id = int.Parse(option.Attributes["MonoScriptID"].Value);
                    GameManager.Instance.IsObserve = true;

                    UIManager.Instance.Dialoue.SetActive(true);
                    DialoguePanel.Instance.Monologue(id);
                }

                if (_action)
                {
                    GameManager.Instance.Event.EndAction();
                }

                if (UIManager.Instance.StartCriminal) UIManager.Instance.StartCriminal = false;
                if (UIManager.Instance.StartGame) UIManager.Instance.StartGame = false;

                gameObject.SetActive(false);
            }
        }
        else
        {
            isTurn = true;
        }

    }


    IEnumerator ShowingDialogue()
    {
        int num = content.Length;

        while (curPos < num && !isTurn)
        {
            txt_chat.text = content.Substring(0, curPos);
            ++curPos;

            yield return new WaitForSeconds(0.05f);
        }

        curPos = 0;
        txt_chat.text = content;
        isTurn = true;
        yield break;
    }
}
