﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CriminalType
{
    Mini,
    Willie,
    Modo,
    Victoria,
    Doctor,
    Final
}

public class CriminalPanel : UIPanel {

    CriminalType _cType;

    [SerializeField]
    RebutDialogPanel _rebutDial;

    GameObject chkPanel;
    Text chkText;

    private void Awake()
    {
        chkPanel = transform.GetChild(4).gameObject;
        chkText = chkPanel.transform.GetChild(2).GetComponent<Text>();
    }

    public void StartCriminal()
    {
        _rebutDial.gameObject.SetActive(true);
        _rebutDial.StartRebut(_cType);

        chkPanel.SetActive(false);

        PopDown();
    }

    public void SelectCriminal(string type)
    {
        _cType = (CriminalType)Enum.Parse(typeof(CriminalType), type);

        string name = null;
        switch(_cType)
        {
            case CriminalType.Doctor:
                name = "Dr.테드";
                break;
            case CriminalType.Modo:
                name = "모도";
                break;
            case CriminalType.Victoria:
                name = "빅토리아";
                break;
            case CriminalType.Willie:
                name = "윌리";
                break;
        }
        chkPanel.SetActive(true);
        chkText.text = "\""+name+"\"를 지목하시겠습니까?";
    }

}
