﻿//------------------------------------------------------------------//
// 작성자 : 조성혜
// 대화 내용, 일러스트 등 조정 
// 최종 수정일자 : 18.09.02dlf
//------------------------------------------------------------------//
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public enum DialogueType { OBJ, NPC, SELECT,HEARING,EXP,DET,ALO}
public class DialoguePanel : UIPanel
{

    private static DialoguePanel _instance = null;
    public static DialoguePanel Instance { get { return _instance; } }

    DialogueType type;
    string curScene;

    // ------------- xml -------------------
    XmlDocument clueDoc;
    XmlNode curNode;

    //--------------- 대화 --------------------
    string content;          // 대화 내용 

    private bool isTurn = false;    //다음 대화
    int nextNum = -1;               //다음 대화 id
    private int curPos = 0;         // 글자 순서 

    //텍스트
    private Text txt_name;          // 캐릭터 이름 
    private Text txt_chat;          // 대화 내용 

    // 이미지
    private RectTransform bg_rect;
    private Image bg_img;

    DetailsCamera _det;
    EventCamera _event;

    bool _action = false;

    bool _isTalk = false;
    public bool IsTalk { get { return _isTalk; } set { _isTalk = value; } }

    private void Awake()
    {
        // 씬에 이미 게임 매니저가 있을 경우
        if (_instance)
        {
            //삭제
            Destroy(gameObject);
            return;
        }

        // 유일한 게임 매니저 
        _instance = this;

        bg_rect = transform.GetChild(0).GetComponent<RectTransform>();
        bg_img = transform.GetChild(0).GetComponent<Image>();
        txt_name = transform.GetChild(2).GetComponent<Text>();
        txt_chat = transform.GetChild(3).GetComponent<Text>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ClickDialogue();
    }
    private void OnEnable()
    {
        clueDoc = new XmlDocument();
        clueDoc.Load(Application.dataPath + "/Play_ob.xml");

        _isTalk = true;

        if (bg_rect.gameObject.activeSelf) bg_rect.gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        Invoke("TalkFalse", 0.3f);
    }

    void TalkFalse() { _isTalk = false; }

    bool _isEvent = false;
    string obj_name;
    // 오브젝트 조사, 캐릭터 대화 
    public void StartDialogue(DialogueType _type, string name)
    {
        type = _type;
        obj_name = name;
        curScene = GameManager.Instance.CurScene.ToString();

        XmlNodeList nameList = null;
        if (_type == DialogueType.OBJ)
            nameList = clueDoc.SelectNodes("Observe/" + curScene + "/OBJ/Info[@ObjNameEng='" + name + "']");
        else if (_type == DialogueType.NPC)
            nameList = clueDoc.SelectNodes("Observe/" + curScene + "/NPC/Info[@ObjNameEng='" + name + "']");
        else if (_type == DialogueType.DET)
            nameList = clueDoc.SelectNodes("Observe/" + curScene + "/DET/Info[@ObjNameEng='" + name + "']");
        else if (_type == DialogueType.EXP)
            nameList = clueDoc.SelectNodes("Observe/" + curScene + "/EXP/Info[@ObjNameEng='" + name + "']");

        // 켜져있는 항목 번호
        List<int> onList = new List<int>();
        // Check 'Default State'
        for (int i = 0; i < nameList.Count; i++)
        {
            string state = nameList[i].SelectSingleNode("State").Attributes["DefaultState"].Value;
            if (state != null)
            {
                if (state == "on")
                {
                    onList.Add(i);
                }
            }
        }

        int index = 0;
        if (onList.Count >= 1)
        {
            int priority = -1;
            for (int i = 0; i < onList.Count; i++)
            {
                string str_pri = nameList[onList[i]].SelectSingleNode("State").Attributes["ObjectPriority"].Value;

                int p = int.Parse(str_pri);

                if (p > priority)
                {
                    index = i;
                    priority = p;
                }
            }
        }

        else
        {
#if UNITY_EDITOR
            Debug.Log("DialoguePanel : StartDialogue() :: onList");
#endif
            GameManager.Instance.IsObserve = false;

            gameObject.SetActive(false);
        }

        curNode = nameList[onList[index]];

        string chName = curNode.SelectSingleNode("ScriptChName").InnerText;

        //확대조사 , 세부조사
        if (chName == "n")
        {
            _isEvent = true;
            if (_type == DialogueType.DET)
            {
                CheckOption();
                GameObject cam = GameManager.Instance.Main;
                DetailsCamera de = cam.GetComponentInChildren<DetailsCamera>();
                de.enabled = true;
                de.DetailsObserve(name);
                gameObject.SetActive(false);
            }
            else
            {
                XmlNode op = curNode.SelectSingleNode("Option");
                if (op.Attributes["NextEventName"] != null)
                {
                    GameManager.Instance.Event.ActiveEvent(op.Attributes["NextEventName"].Value);
                    gameObject.SetActive(false);
                }
            }
        }
        else
        {
            _isEvent = false;
            txt_name.text = chName;
            content = curNode.SelectSingleNode("ScriptChText").InnerText;

            string str_next = curNode.SelectSingleNode("ScriptNextID").InnerText;

            // 다음 스크립트가 있을때
            if (str_next != "n")
            {
                nextNum = int.Parse(str_next);
            }
            else nextNum = -1;

            StartCoroutine(ShowingDialogue());
            CheckOption();

        }
    }

    //선택지 
    public void StartDialogue(DialogueType _type, int id)
    {

        type = _type;
        curScene = GameManager.Instance.CurScene.ToString();

        curNode = null;
        if (type == DialogueType.NPC)
            curNode = clueDoc.SelectSingleNode("Observe/" + curScene + "/NPC/Info[@ID='" + id + "']");
#if UNITY_EDITOR
        else
            Debug.Log(name + ": Dialogue Type Error");
#endif
        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        string str_nextId = curNode.SelectSingleNode("ScriptNextID").InnerText;

        //next id 를 판단해 다음 대사 출력
        if (str_nextId != "n")
        {
            nextNum = int.Parse(str_nextId);
        }
        else nextNum = -1;


        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    public void Monologue(int id)
    {
        curScene = GameManager.Instance.CurScene.ToString();

        curNode = clueDoc.SelectSingleNode("Observe/" + curScene + "/ALO/Info[@ID='" + id + "']");

        if (curNode == null)
        {
            curNode = clueDoc.SelectSingleNode("Observe/none/ALO/Info[@ID='" + id + "']");
        }

        if (curNode.SelectSingleNode("State").Attributes["DefaultState"].InnerText != "on")
        {
#if UNITY_EDITOR
            Debug.Log("DialoguePanel : Monologue() :: Off");
#endif
            GameManager.Instance.IsObserve = false;
            gameObject.SetActive(false);
        }
        else
        {
            txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
            content = curNode.SelectSingleNode("ScriptChText").InnerText;

            string str_next = curNode.SelectSingleNode("ScriptNextID").InnerText;

            // 다음 스크립트가 있을때
            if (str_next != "n")
            {
                nextNum = int.Parse(str_next);
            }
            else nextNum = -1;

            StartCoroutine(ShowingDialogue());
            CheckOption();
        }

    }
    
    void CheckOption()
    {
        XmlNode option = curNode.SelectSingleNode("Option");
        if (option != null)
        {
            // Xml Off
            if (option.Attributes["GetOffStateID"] != null)
            {
                string offStateID = option.Attributes["GetOffStateID"].Value;

                string[] IDs = offStateID.Split('/');

                for (int i = 0; i < IDs.Length; i++)
                {
                    //단서 퀘스트
                    int _id = int.Parse(IDs[i]);

                    //탐색
                    if (_id >= 10000 && _id < 20000)
                        SetObserveState(_id, false);
                    else if (_id >= 20000 && _id < 30000)
                        UIManager.Instance.Select.SetSelectState(_id, false);
                }
            }

            // Xml On
            if (option.Attributes["GetOnStateID"] != null)
            {
                string onStateID = option.Attributes["GetOnStateID"].Value;
                onStateID.Replace(" ", "");
                string[] IDs = onStateID.Split('/');

                for (int i = 0; i < IDs.Length; i++)
                {
                    if (IDs[i].Contains("Map"))
                    {
                        UIManager.Instance.Note.MapOpen(IDs[i]);
                    }
                    else if (IDs[i] == "Change")
                        UIManager.Instance.Note.MapChange();
                    else if (IDs[i].Contains("Q"))
                    {
                        if(type != DialogueType.DET && type != DialogueType.EXP)
                            UIManager.Instance.Quest.AddDefaultQuest(IDs[i]);
                        else
                            UIManager.Instance.Quest.AddObserveQuest(IDs[i]);
                    }
                    else
                    {
                        int _id = int.Parse(IDs[i]);

                        //탐색
                        if (_id >= 10000 && _id < 20000)
                            SetObserveState(_id, true);
                        // 선택지
                        else if (_id >= 20000 && _id < 30000)
                            UIManager.Instance.Select.SetSelectState(_id, true);
                        //단서(증거 증언 세부추론)
                        else if ((_id >= 30000 && _id < 40000) || _id >= 50000)
                            UIManager.Instance.Note.Add(_id);
                    }
                }
            }

            //일러스트
            if (option.Attributes["BGNameTrans"] != null)
            {
                string bgName = option.Attributes["BGNameTrans"].Value;

                // n일경우 종료
                if (bgName == "n")
                    bg_rect.gameObject.SetActive(false);
                //아닌 경우 새로 띄우거나 그림 변경
                else
                {
                    if (!bg_rect.gameObject.activeSelf) bg_rect.gameObject.SetActive(true);

                    bg_img.sprite = Resources.Load<Sprite>("Dial/" + bgName);
                    // 이미지 설정
                    bg_img.SetNativeSize();
                }
                
            }

            //캐릭터 스폰
            if(option.Attributes["ChSpwan"] != null)
            {
                string spwan = option.Attributes["ChSpwan"].Value;
                if (spwan == "P_Room")
                    GameManager.Instance.Event.Spwan = true;
                else
                {
                    GameManager.Instance.Event.StartSpawn(spwan);
                }
            }

            //카메라 제어 
            if (option.Attributes["CamCtrl"] != null)
            {
                string cam = option.Attributes["CamCtrl"].Value;
                if (cam == "n")
                    GameManager.Instance.Event.EndAction();
                else
                {
                    _action = true;
                    GameManager.Instance.Event.StartActionCam(cam);
                }
            }
        }
    }

    void NextScript(int nextID)
    {
        XmlNode node = clueDoc.SelectSingleNode("Observe/" + curScene + "/ETC/Info[@ID='" + nextID + "']");
        if (node == null) node = clueDoc.SelectSingleNode("Observe/none/ETC/Info[@ID='" + nextID + "']");
        content = null;

        if (node.SelectSingleNode("ScriptChName").InnerText == "n")
        {
            _isEvent = true;

            CheckOption();
            GameObject cam = GameManager.Instance.Main;
            DetailsCamera de = cam.GetComponentInChildren<DetailsCamera>();
            de.enabled = true;
            de.DetailsObserve(obj_name);
            gameObject.SetActive(false);
        }
        else
        {
            _isEvent = false;
            txt_name.text = node.SelectSingleNode("ScriptChName").InnerText;
            content = node.SelectSingleNode("ScriptChText").InnerText;

            curNode = node;
            nextNum = -1;

            if (node != null)
            {
                string str_nextId = curNode.SelectSingleNode("ScriptNextID").InnerText;

                //next id 를 판단해 다음 대사 출력
                if (str_nextId != "n")
                {
                    nextNum = int.Parse(str_nextId);
                }
                CheckOption();
                StartCoroutine(ShowingDialogue());

            }
#if UNITY_EDITOR
            else { Debug.Log(name + ": 대화창 nextID 오류"); }
#endif
        }
    }

    public void ClickDialogue()
    {
        if (isTurn)
        {

            isTurn = false;
            if (nextNum != -1)
            {
                NextScript(nextNum);
            }
            else
            {
                if(type == DialogueType.NPC)
                {
                    UIManager.Instance.Select.OffCamera();
                    GameManager.Instance.PlayerEnable(true);
                }

                XmlNode option = curNode.SelectSingleNode("Option");

                //이벤트 
                if (option.Attributes["NextEventName"] != null)
                {
                    GameManager.Instance.Event.ActiveEvent(option.Attributes["NextEventName"].Value);
                }

                if (UIManager.Instance.Quest.IsEndQuest)
                {
                    UIManager.Instance.Quest.IsEndQuest = false;
                    // 세부 조사 중
                    _det = GameObject.FindObjectOfType<DetailsCamera>();
                    if (_det != null)
                        _det.EndDetails();

                    //확대 조사 중
                    _event = GameObject.FindObjectOfType<EventCamera>();
                    if (_event != null && UIManager.Instance.Quest.CheckEndEvent(_event.Qid))
                    {
                        _event.EndEvent();
                    }
                }

                if(_action)
                {
                    GameManager.Instance.Event.EndAction();
                }
                gameObject.SetActive(false);

                if (!GameManager.Instance.bPlayEvent && UIManager.Instance.StartGame)
                {
                    UIManager.Instance.PlayMiniGame();
                }

                GameManager.Instance.StartOffInvoke();

            }
        }
        else
        {
            isTurn = true;
        }

    }

    void SetState()
    {
#if UNITY_EDITOR
        Debug.Log("State :: OFF");
#endif
    }

    IEnumerator ShowingDialogue()
    {
        int num = content.Length;

        while(curPos < num && !isTurn)
        {
            txt_chat.text = content.Substring(0, curPos);
            ++curPos;

            yield return new WaitForSeconds(0.05f);
        }

        curPos = 0;
        txt_chat.text = content;
        isTurn = true;
        yield break;
    }

    public void SetObserveState(int checkId, bool _state)
    {
        string str_state;
        if (_state) str_state = "on";
        else str_state = "off";

        bool isFind = false;

        XmlNodeList sceneList = clueDoc.SelectSingleNode("Observe").ChildNodes;
        foreach (XmlNode scene in sceneList)
        {
            XmlNodeList typeList = scene.ChildNodes;
            foreach (XmlNode type in typeList)
            {
                XmlNode node = type.SelectSingleNode("Info[@ID='" + checkId + "']");
                if(node != null)
                {
                    node.SelectSingleNode("State").Attributes["DefaultState"].Value = str_state;
                    isFind = true;
                    break;
                }
            }
            if (isFind) break;
        }
        clueDoc.Save(Application.dataPath + "/Play_ob.xml");
    }
}
