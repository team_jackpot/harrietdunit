﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

struct Select
{
    public string script;
    public int nextID;
    public string nextScene;
}
public class SelectPanel : MonoBehaviour {

    GameObject[] btn = new GameObject[4];
    Text[] txt = new Text[4];
    UIButton[] scripts = new UIButton[4];

    List<Select> selectList = new List<Select>();
    XmlDocument doc;

    GameObject _cam;

    private void Awake()
    {
        for(int i=0; i<4; i++)
        {
            btn[i] = transform.GetChild(i).gameObject;
            txt[i] = btn[i].GetComponentInChildren<Text>();
            scripts[i] = btn[i].GetComponent<UIButton>();
        }
    }

    public void SetCamera(GameObject ch) { _cam = ch.transform.GetChild(1).gameObject; }
    public void OffCamera() { if (_cam != null) _cam.SetActive(false); _cam = null; }
    public void Init(string name)
    {
        selectList.Clear();

        string curScene = GameManager.Instance.CurScene.ToString();

        doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_select.xml");

        XmlNodeList list = doc.SelectSingleNode("Select/" + curScene + "/" + name).ChildNodes;

        foreach (XmlNode node in list)
        {
            string state = node.Attributes["DefaultState"].Value;
            if (state == "on")
            {
                Select sel = new Select();
                sel.script = node.Attributes["SelectKrText"].Value;
                sel.nextID = int.Parse(node.Attributes["ScriptNextID"].Value);
                if (node.Attributes["NextScene"] != null)
                    sel.nextScene = node.Attributes["NextScene"].Value;
                else sel.nextScene = null;

                selectList.Add(sel);
            }
        }

        if (selectList.Count > 0)
        {
            if (_cam != null) _cam.SetActive(true);
            GameManager.Instance.PlayerEnable(false);
            SetSelect();
        }
        else
        {
            UIManager.Instance.Dialoue.SetActive(true);
            DialoguePanel.Instance.StartDialogue(DialogueType.NPC, name);
        }

    }

    void SetSelect()
    {
        GameManager.Instance.IsObserve = true;
        DialoguePanel.Instance.IsTalk = true;
        for (int i=0; i<selectList.Count; i++)
        {
            btn[i].SetActive(true);
            scripts[i].SetClick();
            txt[i].text = selectList[i].script;
        }
    }

    public void Click(int num)
    {
        foreach (GameObject obj in btn)
            obj.SetActive(false);

        int id = selectList[num].nextID;
        selectList.RemoveAt(num);

        UIManager.Instance.Dialoue.SetActive(true);
        DialoguePanel.Instance.StartDialogue(DialogueType.NPC, id);
    }

    public void SetSelectState(int chkID,bool state)
    {
        doc = new XmlDocument();
        doc.Load(Application.dataPath + "/Play_select.xml");

        bool find = false;
        XmlNodeList sceneList = doc.SelectSingleNode("Select").ChildNodes;
        foreach(XmlNode node in sceneList)
        {
            XmlNodeList chList = node.ChildNodes;
            foreach(XmlNode ch in chList)
            {
                XmlNode n = ch.SelectSingleNode("Sel[@ID='" + chkID + "']");
                if(n != null)
                {
                    if(state)
                     n.Attributes["DefaultState"].Value = "on";
                    else
                        n.Attributes["DefaultState"].Value = "off";
                    find = true;
                    break;
                } 
                if (find)
                    break;
            }
        }

        doc.Save(Application.dataPath + "/Play_select.xml");
    }
}
