﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour {
    
    [SerializeField]
    private Image _img;
    [SerializeField]
    private Text _txt;

    public Color clickColor;
    Color oriColor;
    public Color overColor;

    public bool Change;
    public Sprite[] sp;

    public AudioClip over;
    public AudioClip click;
    AudioSource audio;

    bool isClick = false;

    private void Awake()
    {
        audio = GameManager.Instance.Audio;

        if (_txt != null)
            oriColor = _txt.color;
    }
    public void SetClick()
    {
        isClick = false;
        if(_img != null && Change)
            _img.sprite = sp[0];
        if (_txt != null)
            _txt.color = oriColor;
    }

    //Over
    public void PointerEnter()
    {
        if (over != null)
            audio.PlayOneShot(over);
        if (_img != null)
        {
            if (Change)
            {
                if (!isClick)
                    _img.sprite = sp[1];
            }
            else
                _img.color = Color.white;
        }
        if (_txt != null)
        {
            if (!isClick)
                _txt.color = overColor;
        }
    }

    public void PointerExit()
    {
        if (!isClick)
        {
            if (_img != null)
            {
                if (!Change)
                    _img.color = new Color(1, 1, 1, 0);
                else
                    _img.sprite = sp[0];
            }
            if (_txt != null)
                _txt.color = oriColor;
        }

    }

    public void OnClick()
    {
        isClick = true;
        if(click != null)
            audio.PlayOneShot(click);

        if (_txt != null)
            _txt.color = clickColor;

        if(Change & _img != null)
            _img.sprite = sp[2];
    }
}
